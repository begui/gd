#include <algorithm>
#include <array>

#include "gd/gd_app_sdl.hh"

constexpr auto WIDTH{ 1280 / 2 };
constexpr auto HEIGHT{ 800 / 2 };
constexpr auto TILE{ 16 };
constexpr auto DIM{ TILE - 2 };
constexpr auto SPACE{ TILE - DIM };
constexpr auto NUMOFCELLS{ ( WIDTH / TILE * HEIGHT / TILE ) };
//
constexpr std::array< std::pair< int32_t, int32_t >, 8 > DIRECTIONS{
    { { 0, -1 }, { 0, 1 }, { -1, 0 }, { 1, 0 }, { 1, 1 }, { -1, 1 }, { -1, -1 }, { 1, -1 } } };

//
auto get_position_index = [] ( int width, int x, int y ) { return ( y / TILE ) * ( width / TILE ) + ( x / TILE ); };
//
auto collision = [] ( auto &grid, int x, int y, bool enable ) {
  auto index = get_position_index ( WIDTH, x, y );
  if ( index >= 0 && std::cmp_less(index, grid.size()) ) {
    if ( x >= grid[ index ].bound.x && x && x <= grid[ index ].bound.x + DIM && y >= grid[ index ].bound.y &&
         y <= grid[ index ].bound.y + DIM ) {
      grid[ index ].alive = enable;
    }
  }
};
//
auto collision_clear = [] ( auto &grid ) {
  std::ranges::for_each ( grid.begin ( ), grid.end ( ), [] ( auto &c ) { c.alive = false; } );
};
//
auto update_grid = [] ( auto &grid ) {
  std::ranges::for_each ( grid.begin ( ), grid.end ( ), [] ( auto &c ) {
    c.alive    = c.tmpstate;
    c.tmpstate = false;
  } );
};
//
struct GoL {
  struct Cell {
    sdl::Rectf_t bound;
    bool alive{ false };
    bool tmpstate{ false };
  };
  std::array< Cell, NUMOFCELLS > grid;
  bool simulate{ false };
  int32_t simulationTick{ 30 };
};
//
auto count_live_neighbors ( const auto &grid, int x, int y ) {
  int count{ 0 };
  for ( auto &&[ first, second ] : DIRECTIONS ) {
    const auto xx = x + ( first * TILE );
    const auto yy = y + ( second * TILE );
    if ( const auto index = get_position_index ( WIDTH, xx, yy ); index >= 0 && std::cmp_less(index, grid.size()) ) {
      count += grid[ index ].alive;
    }
  }
  return count;
}
//
void setCellAlive ( auto &grid, auto &c ) {
  if ( c.alive ) {
    c.tmpstate   = c.alive;
    const auto x = c.bound.x;
    const auto y = c.bound.y;
    if ( const auto cnt = count_live_neighbors ( grid, x, y ); cnt < 2 || cnt > 3 ) {
      c.tmpstate = false;
    }
    // for a shortcut, we can check the neighbors to see if any that are dead come back to life
    for ( const auto &[ first, second ] : DIRECTIONS ) {
      const auto xx    = x + ( first * TILE );
      const auto yy    = y + ( second * TILE );
      const auto index = get_position_index ( WIDTH, xx, yy );
      if ( index >= 0 && std::cmp_less(index, grid.size()) ) {
        if ( !grid[ index ].alive ) {
          grid[ index ].tmpstate = ( count_live_neighbors ( grid, xx, yy ) == 3 );
        }
      }
    }
  }
};

class GameOfLife : public gd::SDLApp {
 private:
  GoL gol;

  bool init ( [[maybe_unused]] const int argc, [[maybe_unused]] char *argv[] ) noexcept override {
    Window ( ).title ( "Conways Game of Life" );
    Renderer ( ).setColor ( sdl::Color::WHITE );

    // Init our grid
    float x{ SPACE }, y{ SPACE };
    for ( auto &c : gol.grid ) {
      c.bound = { x, y, DIM, DIM };
      x += DIM + SPACE;
      if ( x + DIM > WIDTH ) {
        x = SPACE;
        y += DIM + SPACE;
      }
    }
    return true;
  }
  void destroy ( ) noexcept override {
    //
  }
  void update ( [[maybe_unused]] float delta ) noexcept override {
    //
    if ( gol.simulate ) {
      for ( auto &v : gol.grid ) {
        setCellAlive ( gol.grid, v );
      }
      update_grid ( gol.grid );
    }
  }
  //
  void render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept override {
    sdl::Color color = sdl::Color::BLACK;
    for ( const auto &g : gol.grid ) {
      ( g.alive ) ? Renderer ( ).drawRectFill ( g.bound, color ) : Renderer ( ).drawRect ( g.bound, color );
    }
  }

 private:
  void onKeyboard ( const SDL_KeyboardEvent &event, const bool isPressed ) noexcept override {
    switch ( event.key ) {
      case SDLK_SPACE:
        if ( !isPressed ) {
          gol.simulate = !gol.simulate;
        }
        break;
      default:
        break;
    }
  }

  void onMouseMotion ( const SDL_MouseMotionEvent &ev ) noexcept override {
    if ( !gol.simulate ) {
      const auto bits = ev.state;
      // We only want ONE mouse button pushed at a time.
      if ( bits && !( bits & ( bits - 1 ) ) ) {
        if ( SDL_BUTTON_LMASK & bits ) {
          collision ( gol.grid, ev.x, ev.y, true );
        }
        if ( SDL_BUTTON_RMASK & bits ) {
          collision ( gol.grid, ev.x, ev.y, false );
        }
      }
    }
  }

  void onMouseButton ( const SDL_MouseButtonEvent &ev, const bool isPressed ) noexcept override {
    if ( !gol.simulate ) {
      if ( isPressed ) {
        switch ( ev.button ) {
          case SDL_BUTTON_LEFT: {
            if ( ev.clicks > 1 ) {
              collision ( gol.grid, ev.x, ev.y, true );
            }
          } break;
          case SDL_BUTTON_RIGHT:
            collision ( gol.grid, ev.x, ev.y, false );
            break;
          case SDL_BUTTON_MIDDLE:
            collision_clear ( gol.grid );
            break;
          default:
            break;
        }
      }
    }
  }
};

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( int argc, char **argv ) {
  auto app = std::make_unique< GameOfLife > ( );
  app->options
      .setWindowSize ( 800, 600 )  //
      .setTicksPerSecond ( 60 )
      .setWindowFlags (                     //
          sdl::WindowFlagType::AlwaysOnTop  //
          )                                 //
      .setLogLevel ( sdl::log::PriorityType::Debug );
  return app;
}
