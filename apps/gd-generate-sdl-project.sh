#!/bin/bash
# --------------------------------------------------------
# This script initializes a new sdl game project structure with 
# predefined files and directories, using the provided game 
# and directory names.
#
# Usage:
#   ./gd-generate-sdl-project.sh
#
# The script will prompt you to:
# 1. Enter a directory name for the project (e.g., gd-project)
# 2. Enter a game name (e.g., My Game)
#
# It then creates the following structure:
#   <directory_name>/
#     ├── src/
#     │   └── main.cc      # Contains basic setup for the game
#     └── CMakeLists.txt   # CMake configuration for the project
#
# Note: 
#    You will need to add this to the root CMakelists.txt inorder to build.
#
# Example:
#   ./gd-generate-sdl-project.sh
#   Enter directory name (e.g., gd-project): gd-my-awesoem-game
#   Enter game name (e.g., My Game): My Awesome Game 
#   Project gd-my-awesome-game created successfully!
# --------------------------------------------------------

# Check for ncurses and prompt user for input
read -p "Enter directory name (e.g., gd-project): " DIR_NAME

# Check if the directory already exists
if [ -d "$DIR_NAME" ]; then
  echo "Project already exists in directory $DIR_NAME."
  exit 1
fi

read -p "Enter game name (e.g., My Game): " GAME_NAME

# Create the directory structure
mkdir -p "$DIR_NAME/src"

# Generate main.cc file
MAIN_CC_FILE="$DIR_NAME/src/main.cc"
CLASS_NAME=$(echo "$GAME_NAME" | tr -d ' ')

cat <<EOL > "$MAIN_CC_FILE"
#include "gd/gd_app_sdl.hh"

class $CLASS_NAME : public gd::SDLApp {
 private:
  bool init ( [[maybe_unused]] const int argc, [[maybe_unused]] char *argv[] ) noexcept override {
    Window ( ).title ( "$GAME_NAME" );
    return true;
  }
  void destroy ( ) noexcept override {
    //
  }
  void update ( [[maybe_unused]] float delta ) noexcept override {
    //
  }
  void render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept override {
    //
  }
};

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( [[maybe_unused]] int argc, [[maybe_unused]] char **argv ) {
  auto app = std::make_unique< $CLASS_NAME > ( );
  app->options
      .setWindowSize ( 800, 600 )  //
      .setTicksPerSecond ( 60 )
      .setWindowFlags (                     //
          sdl::WindowFlagType::AlwaysOnTop  //
          )                                 //
      .setLogLevel ( sdl::log::PriorityType::Debug );
  return app;
}
EOL

# Generate CMakeLists.txt file
CMAKE_FILE="$DIR_NAME/CMakeLists.txt"
PROJECT_NAME=$DIR_NAME

cat <<EOL > "$CMAKE_FILE"
project($PROJECT_NAME VERSION 0.0.0)

message(STATUS \${PROJECT_NAME})

set(GD_USE_IMGUI ON)
set(GD_USE_SDL_TTF OFF)
set(GD_USE_SDL_IMAGE OFF)
set(GD_USE_SDL_MIXER OFF)

set(\${PROJECT_NAME}_SRC
  src/main.cc
  )

add_gd_library(\${PROJECT_NAME}  "\${\${PROJECT_NAME}_SRC}")
EOL

echo "Project $PROJECT_NAME created successfully in directory $DIR_NAME!"
