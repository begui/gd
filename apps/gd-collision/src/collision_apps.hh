#pragma once

#include "gd/gd_app_sdl.hh"
#include "ecs.hh"

struct ICollisionApp {
  virtual void init ( gd::SDLApp& app ) noexcept                            = 0;
  virtual void update ( gd::SDLApp& app ) noexcept = 0;
  virtual void draw ( gd::SDLApp& app) noexcept   = 0;
 protected:
  entt::registry registry_;
};

class CollisionAABB final : public ICollisionApp {
 public:
  void init ( gd::SDLApp& app ) noexcept override;
  void update ( gd::SDLApp& app ) noexcept override;
  void draw ( gd::SDLApp& app ) noexcept override;

  };
