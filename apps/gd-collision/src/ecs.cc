#include "ecs.hh"

namespace ecs {
entt::registry::entity_type make_static_rect ( entt::registry &registry,            //
                                               const component::Size &size,         //
                                               const component::Position &position  //
) {
  //
  auto entity = registry.create ( );

  registry.emplace< component::Renderable > ( entity );
  registry.emplace< component::Size > ( entity, size );
  registry.emplace< component::Position > ( entity, position );

  return entity;
}

entt::registry::entity_type make_player_rect ( entt::registry &registry,  //
                                               const int32_t playerId,
                                               const component::Size &size,          //
                                               const component::Position &position,  //
                                               const component::Velocity &velocity ) {
  //
  auto entity = make_static_rect ( registry, size, position );
  registry.emplace< component::Player > ( entity, playerId );
  registry.emplace< component::Velocity > ( entity, velocity );
  registry.emplace< component::Direction > ( entity, 0.0f );
  return entity;
}

namespace system {

bool collides ( entt::registry &registery, entt::entity playerEntity ) noexcept {
  const auto &position = registery.get< ecs::component::Position > ( playerEntity );
  const auto &size     = registery.get< ecs::component::Size > ( playerEntity );

  const auto wallViewGroup  = registery.group              //
                        < 
                          ecs::component::Renderable,  //
                          ecs::component::Size,       //
                          ecs::component::Position   //
                        >
                        ( entt::get<>, entt::exclude< ecs::component::Player > );

  for ( auto entity : wallViewGroup ) {
    const auto &wallPos  = registery.get< ecs::component::Position > ( entity );
    const auto &wallSize = registery.get< ecs::component::Size > ( entity );
    const auto postRect = sdl::Rectf_t{position.x, position.y, size.w, size.h};
    const auto wallRect = sdl::Rectf_t{wallPos.x, wallPos.y, wallSize.w, wallSize.h};
    if(postRect.hasIntersection( wallRect) ) {
      return true;
    }
  }

  return false;
}
}  // namespace system

}  // namespace ecs
