#include "collision_apps.hh"
#include "gd/gd_app_sdl.hh"

constexpr auto WIDTH{ 800 };
constexpr auto HEIGHT{ 600 };

class CollisionApp final : public gd::SDLApp{
 private:
  bool init (const int argc, char * argv[] ) noexcept override;
  void destroy ( ) noexcept override;
  void update( float delta ) noexcept override;
  void render ( float delta, float interpolation ) noexcept override ;
  void onKeyboard ( const SDL_KeyboardEvent& event, const bool isPressed ) noexcept override;

  CollisionAABB colAABB;
  ICollisionApp* ptr_{ nullptr };
};

bool CollisionApp::init ( const int argc, char* argv[] ) noexcept {
  Window ( ).title ( "GD-Collision" );
  Renderer ( ).setColor( sdl::Color::WHITE );
  colAABB.init ( *this );
  ptr_ = &colAABB;
  //
  return true;
}
void CollisionApp::destroy ( ) noexcept {
  //
}
void CollisionApp::update( [[maybe_unused]]float delta ) noexcept {
  //
  ptr_->update (*this);
}

void CollisionApp::render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept {
  ptr_->draw( *this);
}

void CollisionApp::onKeyboard ( const SDL_KeyboardEvent& event, const bool isPressed ) noexcept {
  switch ( event.key) {
    case SDLK_F1:
      if(isPressed) ptr_ = &colAABB;
      break;
    default:
      break;
  }
}

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( int argc, char** argv ) {
  auto app = std::make_unique< CollisionApp> ( );
  app->options
      .setWindowSize ( 1280, 720 )  //
      .setRendererSize(800 , 600 ) 
      .setWindowFlags (                     //
          // sdl::WindowFlagType::Fullscreen,  //
          sdl::WindowFlagType::AlwaysOnTop  //
          )                                 //
      .setLogLevel ( sdl::log::PriorityType::Debug );
  return app;
}
