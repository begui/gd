#pragma once

#include <entt/entt.hpp>
#include "gd/gd_app_sdl.hh"

namespace ecs {
namespace component {

struct Player {
  int id;
};

struct Position {
  float x{ 0 };
  float y{ 0 };
};

struct Velocity {
  float dx;
  float dy;
};

struct Direction {
  float dir;
};

struct Size {
  float w{ 10 };
  float h{ 10 };
};

struct Renderable {
  uint8_t zIndex{ 0 };
};

}  // namespace component

entt::registry::entity_type make_static_rect ( entt::registry &registry,     //
                                               const component::Size &size,  //
                                               const component::Position &position );

entt::registry::entity_type make_player_rect ( entt::registry &registry,             //
                                               const int32_t playerId,
                                               const component::Size &size,          //
                                               const component::Position &position,  //
                                               const component::Velocity &velocity );
namespace system {
  // 
  bool collides ( entt::registry &registery, entt::entity playerEntity ) noexcept;
}

}  // namespace ecs