#include <cmath>

#include "collision_apps.hh"
#include "gd/utils/gd_math.hh"

constexpr static auto PlayerId{ 1 };
constexpr static auto PlayerSpeedMax{ 2.f };
constexpr static auto PlayerAcceleration{ .2f };
constexpr static auto PlayerFrictionCof{ .96f };
constexpr static auto PlayerSize{ 30 };
constexpr static auto WallSize{ 45 };

void CollisionAABB::init ( gd::SDLApp& app ) noexcept {
  const auto [_,__,ww, hh ] = app.Renderer ( ).getViewPort( );
  const auto cw= ww/2.f ;
  const auto ch= hh/2.f ;
  // create the player
  ecs::make_player_rect ( registry_, PlayerId, { PlayerSize, PlayerSize }, { cw, ch }, { 0, 0 } );
  // create the walls
  for ( size_t i = 0; i < 25; ++i ) {
    float randPosW = gd::math::random< float > ( 0, ( cw - WallSize * 2 ) );
    randPosW       = gd::math::random ( 0, 2 ) == 1 ? randPosW : ww - randPosW - WallSize;
    float randPosH = gd::math::random< float > ( 0, ( ch - WallSize * 2 ) );
    randPosH       = gd::math::random ( 0, 2 ) == 1 ? randPosH : hh - randPosH - WallSize;
    ecs::make_static_rect ( registry_, { WallSize, WallSize }, { randPosW, randPosH } );
  }
}

void CollisionAABB::update ( gd::SDLApp& app) noexcept {
  const auto [ ww, wh ] = app.Renderer ( ).sizef ( );
  auto playerView       = registry_.view< ecs::component::Player,     //
                                    ecs::component::Direction,  //
                                    ecs::component::Size,       //
                                    ecs::component::Position,   //
                                    ecs::component::Velocity > ( );

  if ( sdl::MOUSE::left ( ) ) {

    auto [mouseX,mouseY] = app.Renderer().logicalMousePosition();
    // Update the direction
    for ( const auto entity : playerView ) {
      const auto playerId = playerView.get< ecs::component::Player > ( entity ).id;
      if ( playerId == PlayerId ) {
        const auto& position = playerView.get< ecs::component::Position > ( entity );
        const auto& size     = playerView.get< ecs::component::Size > ( entity );
        auto& direction      = playerView.get< ecs::component::Direction > ( entity );
        auto& velocity       = playerView.get< ecs::component::Velocity > ( entity );

         direction.dir        = std::atan2 ( static_cast< float > ( mouseY) - position.y - ( size.h / 2 ),
                                             static_cast< float > ( mouseX) - position.x - ( size.w / 2 ) );

        velocity.dx += std::clamp ( std::cos ( direction.dir ) * PlayerAcceleration, -PlayerSpeedMax, PlayerSpeedMax );
        velocity.dy += std::clamp ( std::sin ( direction.dir ) * PlayerAcceleration, -PlayerSpeedMax, PlayerSpeedMax );
      }
    }
  }
  for ( const auto entity : playerView ) {
    const auto playerId = playerView.get< ecs::component::Player > ( entity ).id;
    if ( playerId == PlayerId ) {
      auto& position   = playerView.get< ecs::component::Position > ( entity );
      auto& velocity   = playerView.get< ecs::component::Velocity > ( entity );
      const auto& size = playerView.get< ecs::component::Size > ( entity );

      float prevX{ position.x };
      float prevY{ position.y };
      if ( velocity.dx != 0 ) {
        // Keep the player within renderer width
        position.x = std::clamp ( position.x + velocity.dx, 0.f, ww - ( size.w ) );
        if ( ecs::system::collides ( registry_, entity ) ) {
          velocity.dx = 0;
          position.x  = prevX;
        }
      }
      if ( velocity.dy != 0 ) {
        // Keep the player within renderer height
        position.y = std::clamp ( position.y + velocity.dy, 0.f, wh - ( size.h ) );
        if ( ecs::system::collides ( registry_, entity ) ) {
          velocity.dy = 0;
          position.y  = prevY;
        }
      }

      velocity.dx *= PlayerFrictionCof;
      velocity.dy *= PlayerFrictionCof;
    }
  }
}

void CollisionAABB::draw ( gd::SDLApp& app) noexcept {
  // TOOD: rework entt code here
  {
    registry_
        .group<                          //
            ecs::component::Renderable,  //
            ecs::component::Size,        //
            ecs::component::Position >   //
        ( entt::get<>, entt::exclude< ecs::component::Player > )
        .each ( [ &app ] ( auto& r, auto& size, auto& pos ) {
          app.Renderer ( ).drawRectFill ( { pos.x, pos.y, size.w, size.h }, sdl::Color::RED );
          app.Renderer ( ).drawRect ( { pos.x, pos.y, size.w, size.h }, sdl::Color::BLUE );
        } );
  }

  {
    registry_
        .view< ecs::component::Renderable,
               ecs::component::Size,      //
               ecs::component::Position,  //
               ecs::component::Player > ( )
        .each ( [ &app ] ( auto& r, const auto& size, const auto& pos, const auto& player ) {
          app.Renderer ( ).drawRectFill ( { pos.x, pos.y, size.w, size.h }, sdl::Color::BLUE );
          app.Renderer ( ).drawRect ( { pos.x, pos.y, size.w, size.h }, sdl::Color::RED );
        } );
  }
}
