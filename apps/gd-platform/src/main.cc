#include "gd/gd_app_sdl.hh"
#include "ecs.hh"

class GDPlatform : public gd::SDLApp {
 private:
  entt::registry registry;
 private:
  bool init ( [[maybe_unused]] const int argc, [[maybe_unused]] char *argv[] ) noexcept override {
    Window ( ).title ( "GD Platform" );

    // Player
    ecs::make_player(registry,  //
    ecs::component::Position{100.f,100.f});
    // Platform
    auto [rndSizeW,rndSizeH] = Renderer().sizef();
    ecs::make_platform(registry, rndSizeW,rndSizeH, 10);

    return true;
  }
  void destroy ( ) noexcept override {
    //
  }
  void update ( [[maybe_unused]] float delta ) noexcept override {
    //
    ecs::system::collisionSystem(registry);
  }
  void render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept override {
    //
    const auto [screenWidth, screenHeight] = Renderer().size(); 
    ecs::system::inputSystem(registry);
    ecs::system::movementSystem(registry, screenWidth, screenHeight);
    ecs::system::renderSystem(registry, Renderer());
  }
};

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( [[maybe_unused]] int argc, [[maybe_unused]] char **argv ) {
  auto app = std::make_unique< GDPlatform> ( );
  app->options
      .setWindowSize ( 800, 600 )  //
      .setTicksPerSecond ( 60 )
      .setWindowFlags (                     //
          sdl::WindowFlagType::AlwaysOnTop  //
          )                                 //
      .setLogLevel ( sdl::log::PriorityType::Debug );
  return app;
}
