#pragma once

#include <entt/entt.hpp>

#include "gd/gd_app_sdl.hh"
#include "gd/sdl/gd_sdl_types.hh"

namespace ecs {
namespace component {

struct Position {
  float x, y;
};

struct Velocity {
  float dx, dy;
};

struct Player {
  static constexpr float defaultW{32.f};
  static constexpr float defaultH{32.f};
  int score{0};
};

struct Renderable {
  sdl::Colorf color;
  sdl::Rectf_t size;
};

struct Platform {
  static constexpr float defaultW{64.f};
  static constexpr float defaultH{16.f};
};

}  // namespace component

void make_player ( entt::registry &registry,                 //
                   const component::Position &position );

void make_platform ( entt::registry &registry,           //
                     const int32_t screenWidth, const int32_t screenHeight,
                     const int32_t count );

namespace system {
  void inputSystem(entt::registry& registry); 
  void movementSystem(entt::registry& registry, int32_t screenWidth, int32_t screenHeight);
  void collisionSystem(entt::registry& registry);
  void renderSystem ( entt::registry &registry, sdl::Renderer &renderer );
}

}  // namespace ecs