#include "ecs.hh"

#include "gd/sdl/gd_sdl_core.hh"
#include "gd/sdl/gd_sdl_types.hh"
#include "gd/utils/gd_math.hh"

namespace ecs {

void make_player ( entt::registry& registry, const component::Position& position ) {
  auto entity = registry.create ( );
  registry.emplace< component::Renderable > (
      entity, ecs::component::Renderable{ sdl::Colorf::BLUE,
                                          { 0.f, 0.f, component::Player::defaultW, component::Player::defaultH } } );
  registry.emplace< component::Position > ( entity, position );
  registry.emplace< component::Velocity > ( entity, component::Velocity{ .dx = 0.f, .dy = 0.f } );
  registry.emplace< component::Player > ( entity );
}

auto calculate_next_platform_position = [] ( const auto prevPosX, const auto prevPosY, const auto screenWidth ) {
  const float MinVerticalGap   = 50.f;   // Minimum vertical distance the player can jump
  const float MaxVerticalGap   = 100.f;  // Maximum vertical distance the player can jump
  const float MinHorizontalGap = 150.f;  // Minimum horizontal gap between platforms
  float posX                   = gd::math::random ( 0.f, screenWidth - component::Platform::defaultW );
  float posY = prevPosY - gd::math::random ( MinVerticalGap, MaxVerticalGap );  // Ensure upward progression
  // Adjust horizontal position to maintain the minimum gap
  if ( std::abs ( posX - prevPosX ) < MinHorizontalGap ) {
    posX = prevPosX + ( posX > prevPosX ? MinHorizontalGap : -MinHorizontalGap );
  }
  // Ensure posX is within screen bounds
  posX = std::max ( 0.f, std::min ( posX, screenWidth - component::Platform::defaultW ) );

  return std::make_tuple ( posX, posY );
};

void make_platform ( entt::registry& registry, const int32_t screenWidth, const int32_t screenHeight,
                     const int32_t count ) {
  const float StartingHeight = screenHeight - component::Platform::defaultH;  // Starting position near the bottom

  // Initialize the first platform near the bottom of the screen
  float prevPosX = gd::math::random ( 0.f, screenWidth - 68.f );
  float prevPosY = StartingHeight;

  auto firstEntity = registry.create ( );
  registry.emplace< component::Position > ( firstEntity, prevPosX, prevPosY );
  registry.emplace< component::Renderable > (
      firstEntity, component::Renderable{
                       sdl::Colorf::RED, { 0.f, 0.f, component::Platform::defaultW, component::Platform::defaultH } } );
  registry.emplace< component::Platform > ( firstEntity );

  for ( int i = 1; i < count; i++ ) {
    // Calculate positions for the next platform
    auto [ posX, posY ] = calculate_next_platform_position ( prevPosX, prevPosY, screenWidth );
    // Update previous platform's position
    prevPosX = posX;
    prevPosY = posY;
    // Create the new platform
    auto entity = registry.create ( );
    registry.emplace< component::Position > ( entity, posX, posY );
    registry.emplace< component::Renderable > (
        entity, component::Renderable{ sdl::Colorf::RED,
                                       { 0.f, 0.f, component::Platform::defaultW, component::Platform::defaultH } } );
    registry.emplace< component::Platform > ( entity );
  }
}

namespace system {

void movementSystem ( entt::registry& registry, int32_t screenWidth, int32_t screenHeight ) {
  const static auto yHeight{ 256 };
  const static auto velBounce{ -8 };
  const static auto gravity{ .2f };
  auto playerView =
      registry.view< component::Renderable, component::Position, component::Velocity, component::Player > ( );
  for ( auto player : playerView ) {
    auto& playerPos        = playerView.get< component::Position > ( player );
    auto& playerVel        = playerView.get< component::Velocity > ( player );
    const auto& playerRnd  = playerView.get< component::Renderable > ( player );
    const auto& playerSize = playerRnd.size;

    playerVel.dy += gravity;
    playerPos.y += static_cast< int > ( playerVel.dy );

    // Horizontal bounds check
    if ( playerPos.x < 0 )
      playerPos.x = 0;
    if ( playerPos.x > screenWidth - playerSize.w )
      playerPos.x = screenWidth - playerSize.w;

    // Bounce off the bottom of the screen
    if ( playerPos.y > screenHeight - playerSize.h ) {
      playerVel.dy = velBounce;
    }

    // Scroll platforms when player reaches yHeight
    if ( playerPos.y < yHeight ) {
      playerPos.y       = yHeight;
      auto platformView = registry.view< component::Renderable, component::Position, component::Platform > ( );
      for ( auto platform : platformView ) {
        const auto& platformRnd  = platformView.get< component::Renderable > ( platform );
        const auto& platformSize = platformRnd.size;
        auto& platformPos        = platformView.get< component::Position > ( platform );
        platformPos.y -= static_cast< int > ( playerVel.dy );
        if ( platformPos.y > screenHeight ) {
          // Calculate positions for the next platform (lets use the currnet user position)
          auto [ posX, posY ] = calculate_next_platform_position ( playerPos.x, playerPos.y, screenWidth );
          platformPos.x       = posX;  // gd::math::random ( 0.f, screenWidth - platformSize.w );
          platformPos.y       = 0;
        }
      }
    }
  }
}

void inputSystem ( entt::registry& registry ) {
  auto view = registry.view< component::Position, component::Player > ( );
  for ( auto entity : view ) {
    auto& pos = view.get< component::Position > ( entity );

    if ( sdl::KB::right ( ) )
      pos.x += 4;
    if ( sdl::KB::left ( ) )
      pos.x -= 4;
  }
}

void collisionSystem ( entt::registry& registry ) {
  const static int velBounce = -10;
  auto playerView =
      registry.view< component::Renderable, component::Position, component::Velocity, component::Player > ( );
  auto platformView = registry.view< component::Renderable, component::Position, component::Platform > ( );

  for ( auto player : playerView ) {
    const auto& playerSize = playerView.get< component::Renderable > ( player ).size;
    const auto& playerPos  = playerView.get< component::Position > ( player );
    auto& playerVel        = playerView.get< component::Velocity > ( player );

    for ( auto platform : platformView ) {
      const auto& platformSize = playerView.get< component::Renderable > ( platform ).size;
      const auto& platformPos  = platformView.get< component::Position > ( platform );

      if ( ( playerPos.x + playerSize.w > platformPos.x ) && ( playerPos.x < platformPos.x + platformSize.w ) &&
           ( playerPos.y + playerSize.h > platformPos.y ) && ( playerPos.y < platformPos.y ) && ( playerVel.dy > 0 ) ) {
        playerVel.dy = velBounce;
      }
    }
  }
}

void renderSystem ( entt::registry& registry, sdl::Renderer& renderer ) {
  auto view = registry.view< component::Position, component::Renderable > ( );
  for ( auto entity : view ) {
    const auto& pos        = view.get< component::Position > ( entity );
    const auto& renderable = view.get< component::Renderable > ( entity );

    sdl::Rectf_t rect = { pos.x, pos.y, renderable.size.w, renderable.size.h };
    renderer.drawRectFill ( rect, renderable.color );
  }
}

}  // namespace system

}  // namespace ecs