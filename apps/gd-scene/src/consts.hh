#pragma once
#include <cstdint>
#include <type_traits>
//
#include "gd/gd_app_sdl.hh"
#include "gd/utils/gd_math.hh"
//
enum class SceneIdType : int32_t {
  None = 0,
  Scene1,
  Scene2,
  Scene3,
};

constexpr int32_t getSceneDirection(SceneIdType fromScene , SceneIdType toScene) {
    using underlying_type = std::underlying_type_t<SceneIdType>;
    const auto currentIndex = static_cast<underlying_type>(fromScene);
    const auto targetIndex = static_cast<underlying_type>(toScene);

    if (currentIndex == targetIndex) {
        return 0;
    }
    return (targetIndex > currentIndex) ? 1 : -1;
}
//
enum class SceneTransitionType {
  None = 0,
  Classic,
  Blend,
  FadeIn,
  Horizontal,
  Vertical
};
//
//
static constexpr auto sceneGenerator = gd::math::PseudoRandomGenerator(3723992);
static constexpr gd::cid_t SCENE_FROM_ID = sceneGenerator.next_n(1);
static constexpr gd::cid_t SCENE_TO_ID   = sceneGenerator.next_n(2);