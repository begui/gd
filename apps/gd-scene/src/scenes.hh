#pragma once

#include <algorithm>

#include "consts.hh"
#include "gd/gd_app_sdl.hh"
#include "gd/sdl/gd_sdl_renderer.hh"
#include "gd/utils/gd_type_traits.hh"

class Scene1 : public gd::SDLApp::Scene {
 public:
  using gd::SDLApp::Scene::Scene;  // Inherit constructors from the base class

  int32_t id ( ) const noexcept override { return gd::typetrait_util::to_type ( SceneIdType::Scene1 ); }

  static const sdl::Colorf& BackgroundColor ( ) {
    static const sdl::Colorf color = sdl::Colorf::RED;
    return color;
  }

  void onEnter ( ) noexcept override {
    //
  }
  void onExit ( ) noexcept override {
    //
  }
  void update ( float dt ) noexcept override {
    //
  }
  void updateFixed ( float dt ) noexcept override {
    //
  }
  void render ( ) const noexcept override {
    //
    app ( ).Renderer ( ).drawBackground ( BackgroundColor ( ) );
  }
};

class Scene2 : public gd::SDLApp::Scene {
 public:
  using gd::SDLApp::Scene::Scene;  // Inherit constructors from the base class

  int32_t id ( ) const noexcept override { return gd::typetrait_util::to_type ( SceneIdType::Scene2 ); }

  static const sdl::Colorf& BackgroundColor ( ) {
    static const sdl::Colorf color = sdl::Colorf::GREEN;
    return color;
  }

  void onEnter ( ) noexcept override {
    //
  }
  void onExit ( ) noexcept override {
    //
  }
  void update ( float dt ) noexcept override {
    //
  }
  void updateFixed ( float dt ) noexcept override {
    //
  }
  void render ( ) const noexcept override {
    //
    // app ( ).Renderer ( ).setColor( BackgroundColor ( ) );
    app ( ).Renderer ( ).drawBackground ( BackgroundColor ( ) );
  }
};

class Scene3 : public gd::SDLApp::Scene {
 public:
  using gd::SDLApp::Scene::Scene;  // Inherit constructors from the base class

  int32_t id ( ) const noexcept override { return gd::typetrait_util::to_type ( SceneIdType::Scene3 ); }

  static const sdl::Colorf& BackgroundColor ( ) {
    static const sdl::Colorf color = sdl::Colorf::BLUE;
    return color;
  }

  void onEnter ( ) noexcept override {
    //
  }
  void onExit ( ) noexcept override {
    //
  }
  void update ( float dt ) noexcept override {
    //
  }
  void updateFixed ( float dt ) noexcept override {
    //
  }
  void render ( ) const noexcept override {
    //
    app ( ).Renderer ( ).drawBackground ( BackgroundColor ( ) );
  }
};

const sdl::Colorf& GetSceneBackgroundColor ( gd::id_t sceneId ) {
  switch ( static_cast< SceneIdType > ( sceneId ) ) {
    using enum SceneIdType;
    case Scene1:
      return Scene1::BackgroundColor ( );
    case Scene2:
      return Scene2::BackgroundColor ( );
    case Scene3:
      return Scene3::BackgroundColor ( );
    default:
      static const sdl::Colorf defaultColor = sdl::Colorf::BLACK;
      return defaultColor;
  }
}

// Helper function to mix colors based on a progress value (0.0f to 1.0f)
sdl::Colorf MixColors ( const sdl::Colorf& color1, const sdl::Colorf& color2, float progress ) {
  float r = ( color1.r * ( 1.0f - progress ) + color2.r * progress );
  float g = ( color1.g * ( 1.0f - progress ) + color2.g * progress );
  float b = ( color1.b * ( 1.0f - progress ) + color2.b * progress );
  float a = ( color1.a * ( 1.0f - progress ) + color2.a * progress );
  return { r, g, b, a };
}

class BlendTransition : public gd::SDLApp::SceneTransition {
 private:
  const float duration_;         // Transition duration in seconds
  const sdl::Colorf fromColor_;  // Color to fade from
  const sdl::Colorf toColor_;    // Color to fade to
  float timer_{ 0.f };           // Current time elapsed

 public:
  BlendTransition ( gd::SDLApp& app, float duration, const sdl::Colorf& fromColor, const sdl::Colorf& toColor )
      : gd::SDLApp::SceneTransition ( app ), duration_ ( duration ), fromColor_ ( fromColor ), toColor_ ( toColor ) {}

  void onEnter ( ) noexcept override {
    // empty
  }

  void onExit ( ) noexcept override {
    // empty
  }

  void update ( float dt ) override { timer_ += dt; }

  void render ( ) const override {
    float progress           = std::min ( timer_ / duration_, 1.0f );
    sdl::Colorf currentColor = MixColors ( fromColor_, toColor_, progress );
    // app ( ).Renderer ( ).drawColor ( currentColor );
    app ( ).Renderer ( ).drawBackground ( currentColor );
  }

  bool isComplete ( ) const override {
    return timer_ >= duration_;  // Check if the transition is complete
  }
};

class FadeInOutTransition : public gd::SDLApp::SceneTransition {
 private:
  const float duration_;  // Transition duration in seconds
  const bool fadeIn_;     // Whether it's a fade-in transition, otherwise fadeout
  float timer_{ 0.f };    // Current time elapsed

 public:
  FadeInOutTransition ( gd::SDLApp& app, float duration, bool fadeIn = true )
      : gd::SDLApp::SceneTransition ( app ), duration_ ( duration ), fadeIn_ ( fadeIn ) {}

  void onEnter ( ) noexcept override {
    const auto [ w, h ] = app ( ).Renderer ( ).size ( );
    ///
    const auto& scene = getFromScene ( );

    app ( ).Renderer ( ).loadTargetTexture ( fadeIn_ ? SCENE_TO_ID : SCENE_FROM_ID, w, h, [ &scene ] ( ) {
      //
      if ( auto lock = scene.lock ( ) ) {
        lock->render ( );
      }
    } );

    // app ( ).Renderer ( ).textureBlendMode ( SCENE_FROM_ID, sdl::BlendmodeType::Blend);
  }

  void onExit ( ) noexcept override {
    //
    app ( ).Renderer ( ).unloadTexture ( fadeIn_ ? SCENE_TO_ID : SCENE_FROM_ID );
  }

  void update ( float dt ) override {
    timer_ += dt;
    const float progress = std::min ( timer_ / duration_, 1.0f );
    const float alpha    = fadeIn_ ? ( 1.0f - progress ) : progress;  // Adjust alpha based on fade direction
    app ( ).Renderer ( ).textureAlphaf ( SCENE_TO_ID, alpha );
  }

  void render ( ) const override { app ( ).Renderer ( ).drawTexture ( SCENE_TO_ID ); }

  bool isComplete ( ) const override {
    return timer_ >= duration_;  // Check if the transition is complete
  }
};

class DualAxisTransition : public gd::SDLApp::SceneTransition {
 public:
  enum class Orientation {
    Horizontal,
    Vertical
  };

 private:
  const float duration_;
  const float direction_;
  const Orientation orientation_;
  float elapsedTime_{ 0.f };

  float xxf{ 0.f };
  float yyf{ 0.f };
  float xxt{ 0.f };
  float yyt{ 0.f };

  float iStart{ 0.f };
  float iTarget{ 0.f };

 public:
  DualAxisTransition ( gd::SDLApp& app, float duration, float direction, Orientation orientation )
      : gd::SDLApp::SceneTransition ( app ),
        duration_ ( duration ),
        direction_{ direction },
        orientation_{ orientation } {}

  void onEnter ( ) noexcept override {
    elapsedTime_  = 0.f;
    auto [ w, h ] = app ( ).Renderer ( ).size ( );
    ///
    bool rtn {false};
    const auto& sceneFrom = getFromScene ( );
    rtn                   = app ( ).Renderer ( ).loadTargetTexture ( SCENE_FROM_ID, w, h, [ &sceneFrom ] ( ) {
      //
      if ( auto lock = sceneFrom.lock ( ) ) {
        lock->render ( );
      }
    } );
    const auto& sceneTo   = getToScene ( );
    rtn                   = app ( ).Renderer ( ).loadTargetTexture ( SCENE_TO_ID, w, h, [ &sceneTo ] ( ) {
      //
      if ( auto lock = sceneTo.lock ( ) ) {
        lock->render ( );
      }
    } );

    ///
    auto [ wf, hf ] = app ( ).Renderer ( ).sizef ( );
    if ( Orientation::Horizontal == orientation_ ) {
      iStart  = 0.f;
      iTarget = wf;
    }
    if ( Orientation::Vertical == orientation_ ) {
      iStart  = 0.f;
      iTarget = hf;
    }
  }

  void onExit ( ) noexcept override {
    //
  }

  void update ( float dt ) override {
    elapsedTime_ += dt;
    // progress should be between 0 and 1.
    const float progress = std::min ( elapsedTime_ / duration_, 1.0f );
    if ( Orientation::Horizontal == orientation_ ) {
      xxf = iStart + direction_ * ( iTarget - iStart ) * progress;
      xxt = xxf + ( -direction_ * iTarget );
    }

    if ( Orientation::Vertical == orientation_ ) {
      yyf = iStart + direction_ * ( iTarget - iStart ) * progress;
      yyt = yyf + ( -direction_ * iTarget );
    }
  }

  void render ( ) const override {
    const auto [ w, h ] = app ( ).Renderer ( ).sizef ( );
    //
    // Changing the order of this rendreing avoids a flicker
    sdl::Rectf_t sceneToRect   = { xxt, yyt, w, h };
    app ( ).Renderer ( ).drawTexture ( SCENE_TO_ID, std::nullopt, sceneToRect );
    sdl::Rectf_t sceneFromRect = { xxf, yyf, w, h };
    app ( ).Renderer ( ).drawTexture ( SCENE_FROM_ID, std::nullopt, sceneFromRect );
  }

  bool isComplete ( ) const override {
    return elapsedTime_ >= duration_;  // Check if the transition is complete
  }
};