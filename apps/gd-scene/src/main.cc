#include <cstdlib>

#include "gd/gd_app_sdl.hh"
#include "scenes.hh"

#include <iostream>

class ScenesApp final : public gd::SDLApp {
 private:
  SceneIdType currentSceneIdType_{ SceneIdType::None };

  bool init ( [[maybe_unused]] const int argc, [[maybe_unused]] char* argv[] ) noexcept override {
    //
    Window ( ).title ( "Scenes" );

    currentSceneIdType_       = SceneIdType::Scene1;
    const auto currentSceneId = gd::typetrait_util::to_type ( currentSceneIdType_ );
    changeSceneTo ( currentSceneId );


    std::cout << SCENE_FROM_ID << std::endl;
    std::cout << SCENE_TO_ID << std::endl;

    return true;
  }
  void destroy ( ) noexcept override {
    //
  }

  void update ( [[maybe_unused]] float delta ) noexcept override {}

  void render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept override {
#if GD_USE_IMGUI

    ImGui::Begin ( "Scene Selection" );

    static constexpr const char* sceneTransition[] = { "Fixed", "Blend", "Fade", "Horizontal", "Vertical" };
    static int currentSceneTransition              = 0;
    if ( ImGui::Combo ( "Select Transition", &currentSceneTransition, sceneTransition,
                        IM_ARRAYSIZE ( sceneTransition ) ) ) {
      // empty
    }

    ImGui::Separator ( );

    static constexpr const char* sceneItems[] = { "Scene 1", "Scene 2", "Scene 3" };
    if ( static int currentScene = 0;
         ImGui::Combo ( "Select Scene", &currentScene, sceneItems, IM_ARRAYSIZE ( sceneItems ) ) ) {
      // Assuming Scene1 = 1, Scene2 = 2, etc.
      const auto sceneId = static_cast< SceneIdType > ( currentScene + 1 );
      const auto transId = static_cast< SceneTransitionType > ( currentSceneTransition + 1 );
      updateScene ( sceneId, transId );
    }

    ImGui::End ( );

#endif
  }

  void updateScene ( SceneIdType sceneId, SceneTransitionType sceneTransition ) {
    using enum SceneTransitionType;
    switch ( sceneTransition ) {
      case Classic:
        changeToScene ( sceneId );
        break;
      case Blend:
        changeToSceneWithBlend ( sceneId );
        break;
      case FadeIn:
        changeToSceneWithFade ( sceneId );
        break;
      case Horizontal:
        changeToSceneWithDualAxis ( sceneId, DualAxisTransition::Orientation::Horizontal );
        break;
      case Vertical:
        changeToSceneWithDualAxis ( sceneId, DualAxisTransition::Orientation::Vertical );
        break;
      default:
        break;
    }
  }

  void changeToScene ( SceneIdType sceneId ) {
    currentSceneIdType_       = sceneId;
    const auto currentSceneId = gd::typetrait_util::to_type ( currentSceneIdType_ );
    changeSceneTo ( currentSceneId );
  }

  void changeToSceneWithBlend ( SceneIdType toScene ) {
    const auto fromScene   = currentSceneIdType_;
    const auto fromSceneId = gd::typetrait_util::to_type ( fromScene );

    currentSceneIdType_       = toScene;
    const auto currentSceneId = gd::typetrait_util::to_type ( currentSceneIdType_ );

    changeSceneTo ( currentSceneId,
                    std::make_shared< BlendTransition > ( *this, 1.f, GetSceneBackgroundColor ( fromSceneId ),
                                                          GetSceneBackgroundColor ( currentSceneId ) ) );
  }

  void changeToSceneWithFade ( SceneIdType toScene ) {
    currentSceneIdType_       = toScene;
    const auto currentSceneId = gd::typetrait_util::to_type ( currentSceneIdType_ );

    changeSceneTo ( currentSceneId, std::make_shared< FadeInOutTransition > ( *this, 1.f ) );
  }

  void changeToSceneWithDualAxis ( SceneIdType toScene, DualAxisTransition::Orientation ori ) {
    const auto fromScene = currentSceneIdType_;
    const auto direction = getSceneDirection ( fromScene, toScene );

    currentSceneIdType_           = toScene;
    const auto currentSceneTypeId = gd::typetrait_util::to_type ( currentSceneIdType_ );

    changeSceneTo ( currentSceneTypeId, std::make_shared< DualAxisTransition > ( *this, 3.f, direction, ori ) );
  }
};

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( int argc, char** argv ) {
  auto app = std::make_unique< ScenesApp > ( );
  app->options
      .setWindowSize ( 800, 600 )  //
      .setTicksPerSecond ( 60 )
      .setWindowFlags (  //
          sdl::WindowFlagType::AlwaysOnTop,
          sdl::WindowFlagType::Resizeable )  //
      .setLogLevel ( sdl::log::PriorityType::Debug );

  app->addScene< Scene1 > ( )  //
      .addScene< Scene2 > ( )  //
      .addScene< Scene3 > ( );

  return app;
}