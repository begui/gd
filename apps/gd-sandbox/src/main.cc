#include <iostream>

#include "ecs.hh"
#include "gd/gd_app_sdl.hh"
#include "gd/utils/gd_math.hh"

class Sandbox : public gd::SDLApp {
  bool init ( [[maybe_unused]] const int argc, [[maybe_unused]] char* argv[] ) noexcept override {
    Window ( ).title ( "Sandbox" );

    {
      const auto [ w, h ] = Renderer ( ).sizef ( );
      for ( auto i{ 0 }; i < 100; ++i ) {
        const auto ww = gd::math::random ( 0.f, w );
        const auto hh = gd::math::random ( 0.f, h );
        ecs::entity::make_rect ( registry, { ww, hh } );
      }
      for ( auto i{ 0 }; i < 1000; ++i ) {
        ecs::entity::make_star ( registry, w, h );
      }
    }

    constexpr std::array< std::pair< int32_t, int32_t >, 8 > DIRECTIONS{
        { { 0, -1 }, { 0, 1 }, { -1, 0 }, { 1, 0 }, { 1, 1 }, { -1, 1 }, { -1, -1 }, { 1, -1 } } };

    {
      auto [x,y, w, h ] = Renderer ( ).getViewPort( );
      for ( const auto &[ dirX, dirY ] : DIRECTIONS ) {
        ecs::entity::make_rect ( registry, { w/2.f, h/2.f }, { { dirX, dirY } } );
      }
    }

    ecs::entity::make_triangle ( registry );

    return true;
  }
  void destroy ( ) noexcept override {
    //
    std::cout << "Destroy" << std::endl;
  }

  void update ( [[maybe_unused]] float delta ) noexcept override {
    //
    const auto [ w, h ] = Renderer ( ).size ( );
    ecs::system::update_directional ( registry, w, h );
    ecs::system::update_stars ( registry, w, h );
    ecs::system::update_positions ( registry );
  }
  void render ( [[maybe_unused]] float delta, [[maybe_unused]] float interpolation ) noexcept override {
    {
#if GD_USE_IMGUI
      ImGui::Begin ( "Hello, world!" );  // Create a window called "Hello, world!" and append into it.

      ImGui::Text ( "Average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO ( ).Framerate,
                    ImGui::GetIO ( ).Framerate );
      if ( static auto clearcolor = ImVec4 ( 0.f, 0.f, 0.f, 1.0f );
           ImGui::ColorEdit3 ( "Clear Color", ( float* ) &clearcolor ) ) {
        Renderer ( ).setColor ( sdl::Colorf{ clearcolor.x, clearcolor.y, clearcolor.z, clearcolor.w } );
      }

      ImGui::End ( );
#endif
    }

    // Render the red squares
    registry
        .view< ecs::component::Renderable, ecs::component::Rectangle,
               ecs::component::PositionState > ( )  //
        .each ( [ this ] ( [[maybe_unused]] auto& r, const auto& rect, const auto& pos ) {

          Renderer ( ).drawRect ( { pos.curr.x, pos.curr.y, rect.w, rect.h }, r.color );
        } );

    // Render Triangle
    registry
        .view< ecs::component::Renderable, ecs::component::Star > ( )  //
        .each ( [ this ] ( [[maybe_unused]] auto& r, const auto& star ) {
          Renderer ( ).drawPoint ( { star.x, star.y }, sdl::Color{ star.color, star.color, 0xff, 0xff } );
        } );

    // Render Triangle
    registry
        .view< ecs::component::Renderable, ecs::component::Triangle > ( )  //
        .each ( [ this ] ( [[maybe_unused]] auto& r, const auto& vert ) { Renderer ( ).drawGeometry ( vert.vert ); } );

    // Render Boarder
    const auto [ w, h ] = Renderer ( ).sizef ( );
    //// Top
    Renderer ( ).drawRectFill ( { 0, 0, w, 2 }, sdl::Color::WHITE );
    //// Bottom
    Renderer ( ).drawRectFill ( { 0, h - 2, w, 2 }, sdl::Color::WHITE );
    //// Left
    Renderer ( ).drawRectFill ( { 0, 0, 2, h }, sdl::Color::WHITE );
    //// Right
    Renderer ( ).drawRectFill ( { w - 2, 0, 2, h }, sdl::Color::WHITE );
  }

 private:
  entt::registry registry;
};

std::unique_ptr< sdl::SDLAppMain > sdl::SDLAppMain::init ( [[maybe_unused]] int argc, [[maybe_unused]] char** argv ) {
  auto app = std::make_unique< Sandbox > ( );
  app->options
      .setWindowSize ( 800, 600 )  //
      .setTicksPerSecond ( 60 )
      .setWindowFlags (                     //
          sdl::WindowFlagType::AlwaysOnTop  //
          )                                 //
      .setLogLevel ( sdl::log::PriorityType::Debug );
  return app;
}
