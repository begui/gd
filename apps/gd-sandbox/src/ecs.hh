#pragma once
#include <entt/entt.hpp>
#include <optional>
#include "gd/sdl/gd_sdl_types.hh"


namespace ecs {
namespace component {

struct Renderable {
  uint8_t zIndex;
  sdl::Color color;
};

struct Position {
  float x{ 0 };
  float y{ 0 };
};

struct PositionState {
  Position curr;
  Position prev;
};

struct Velocity {
  float dx;
  float dy;
};

struct Rotation {
  float angle{ 0.f };
  int centerX{ 0 };
  int centerY{ 0 };
};

struct Directional {
  int vDir{ 1 };
  int hDir{ 1 };
};

struct Rectangle {
  float w{ 10 };
  float h{ 10 };
};

struct Star {
  float x{ 0 };
  float y{ 0 };
  uint8_t color{ 0x00 };
};

struct Triangle {
  std::vector< sdl::Vertex > vert;
  // sdl::Vertex vert[3];
};

}  // namespace component

namespace entity {
entt::registry::entity_type make_rect ( entt::registry &registry, const component::Position &position,
                                        std::optional< component::Directional > dir = std::nullopt );
entt::registry::entity_type make_star ( entt::registry &registry, const float width, const float height );
entt::registry::entity_type make_triangle( entt::registry &registry );
}  // namespace entity
namespace system {
void update_positions ( entt::registry &registry );
void update_directional ( entt::registry &registry, int w, int h );
void update_stars ( entt::registry &registery, int w, int h );
}  // namespace system

}  // namespace ecs