#include "ecs.hh"

#include <random>

#include "gd/utils/gd_math.hh"


namespace ecs {
namespace entity {


entt::registry::entity_type make_triangle( entt::registry &registry ) {
  auto entity = registry.create ( );
  registry.emplace< component::Renderable > ( entity );

  auto &tri = registry.emplace< component::Triangle> ( entity );
  //
  tri.vert.emplace_back( sdl::Pointf_t{ 400.f, 150.f }, sdl::Colorf::RED );
  //  left
  tri.vert.emplace_back( sdl::Pointf_t{ 200.f, 450.f }, sdl::Colorf::GREEN );
  //  right
  tri.vert.emplace_back( sdl::Pointf_t{ 600.f, 450.f }, sdl::Colorf::BLUE );

  return entity;
}

entt::registry::entity_type make_rect ( entt::registry &registry, const component::Position &position,
                                        std::optional< component::Directional > dir ) {
  auto entity = registry.create ( );

  registry.emplace< component::Renderable > ( entity, 0, sdl::Color{255, 0, 0, 255} );
  registry.emplace< component::Rectangle > ( entity );
  registry.emplace< component::PositionState > ( entity, position, position );

  if ( !dir.has_value ( ) ) {
    auto vdir = gd::math::random ( 0, 1 );
    auto hdir = gd::math::random ( 0, 1 );
    dir       = std::make_optional< component::Directional > ( vdir == 0 ? -1 : 1, hdir == 0 ? -1 : 1 );
  }
  registry.emplace< component::Directional > ( entity, dir.value ( ) );
  return entity;
}

entt::registry::entity_type make_star ( entt::registry &registry, const float width, const float height ) {
  auto entity = registry.create ( );

  const auto calculate_initial_point = [] ( auto dim ) {  //
    return 1.0f * gd::math::random ( 0.f, static_cast< float > ( dim ) );
  };

  const auto x = calculate_initial_point ( width );
  const auto y = calculate_initial_point ( height );
  registry.emplace< component::Star > ( entity, x, y );
  registry.emplace< component::Renderable > ( entity );

  return entity;
}

}  // namespace entity
namespace system {
void update_positions ( entt::registry &registry ) {
  registry.view< component::Directional, component::PositionState > ( ).each (
      [] ( auto &directional, auto &position ) {
        position.prev.x = position.curr.x;
        position.prev.y = position.curr.y;
        position.curr.x += static_cast< float > ( directional.hDir );
        position.curr.y += static_cast< float > ( directional.vDir );
      } );
}
void update_directional ( entt::registry &registry, int32_t w, int32_t h ) {
  registry.view< component::Directional, component::PositionState > ( ).each (
      [ w, h ] ( auto &directional, auto &position ) {
        directional.hDir *= gd::math::in_range( 0, w, position.curr.x ) ? 1 : -1;
        directional.vDir *= gd::math::in_range( 0, h, position.curr.y ) ? 1 : -1;
      } );
}

void update_stars ( entt::registry &registry, int32_t width, int32_t height ) {

  const auto widthf = static_cast<float>(width);
  const auto heightf= static_cast<float>(height);

  const auto calculate_initial_point = [] ( auto dim ) {  //
    return 1.0f * gd::math::random ( 0.f, static_cast< float > ( dim ) );
  };

  const auto  calculate_updated_point = [] ( auto a, auto dim, auto color ) {
    return ( a - dim / 2.f ) * ( color * 0.00001f + 1.f ) + dim / 2.f;
  };

  auto view = registry.view< component::Star > ( );
  // returns entity and reference to it's components
  for( auto [entity, star] : view.each()) {
    if ( star.color < 0xff ) {
      star.color += 1;
    }
    star.x = calculate_updated_point ( star.x, widthf, star.color );
    star.y = calculate_updated_point ( star.y, heightf, star.color );
    if ( star.x < 0.f || star.x > widthf || star.y < 0.f || star.y > heightf ) {
      // reset the position 
      star.x     = calculate_initial_point ( width );
      star.y     = calculate_initial_point ( height );
      star.color = 0x00;
    }
  }
}

}  // namespace system
}  // namespace ecs