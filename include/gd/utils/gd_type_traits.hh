#pragma once
#include <type_traits>

namespace gd {

template < typename T >
concept FloatOrUInt8 = std::is_same_v< T, float > || std::is_same_v< T, uint8_t >;
}

namespace gd::typetrait_util {

template < typename T >
constexpr auto to_type ( T t ) {
  return static_cast< typename std::underlying_type_t< T > > ( t );
}

template < typename TO, typename T >
constexpr TO to_type ( T t ) {
  return static_cast< TO > ( to_type ( t ) );
}

template < typename H, typename... T >
using are_same = std::conjunction< std::is_same< H, T >... >;

}  // namespace gd::typetrait_util
