#pragma once

#include <algorithm>
#include <array>
#include <cstdint>
#include <numeric>
#include <ostream>

namespace gd {

struct FrameData {
  explicit FrameData ( const int32_t ticks = 60 ) : ticksPerSecond{ ticks } {}
  const int32_t ticksPerSecond;
  const float fixedDelta = 1.f / static_cast< float > ( ticksPerSecond );
  float frameDelta       = { 0.f };
  uint32_t frameTick{ 0 };
  uint32_t fps{ 0 };
  uint64_t updateTick{ 0 };
  uint64_t updateFixedTick{ 0 };
  uint64_t frameAccumulator{ 0 };
  uint64_t frameCurrentTime{ 0 };
  uint64_t framePreviousTime{ 0 };
  uint64_t frameDeltaTime{ 0 };
  uint64_t frameDeltaAvgTime{ 0 };
};

inline std::ostream &operator<< ( std::ostream &os, const FrameData &frameData ) {
  os << "ticksPerSecond: " << frameData.ticksPerSecond << "\n";
  os << "fps: " << frameData.fps << "\n";
  os << "fixedDelta: " << frameData.fixedDelta << "\n";
  os << "frameDelta: " << frameData.frameDelta << "\n";
  os << "frameTick: " << frameData.frameTick << "\n";
  os << "updateTick: " << frameData.updateTick << "\n";
  os << "updateFixedTick: " << frameData.updateFixedTick << "\n";
  os << "frameAccumulator: " << frameData.frameAccumulator << "\n";
  os << "frameCurrentTime: " << frameData.frameCurrentTime << "\n";
  os << "framePreviousTime: " << frameData.framePreviousTime << "\n";
  os << "frameDeltaTime: " << frameData.frameDeltaTime << "\n";
  os << "frameDeltaAvgTime: " << frameData.frameDeltaAvgTime << "\n";
  return os;
}

template < typename T >
concept GameImpl = requires ( T game, const FrameData &frd ) {
  { game.gameRunning ( ) } -> std::convertible_to< bool >;
  { game.gameEvent ( ) };
  { game.gameUpdate ( float{ } ) };
  { game.gameUpdateFixed ( float{ } ) };
  { game.gameRender ( frd ) };
};

template < typename T >
concept GameImplCounter = requires ( T deltaTimer ) {
  { deltaTimer.counter ( ) } -> std::convertible_to< uint64_t >;
  { deltaTimer.frequency ( ) } -> std::convertible_to< uint64_t >;
  { deltaTimer.tick ( ) } -> std::convertible_to< uint64_t >;
};

class GameLoop final {
 public:
  void run ( GameImpl auto &game, GameImplCounter auto counter, FrameData frd ) const noexcept {
    const uint64_t FRAME_DELTA_TIME{ counter.frequency ( ) / frd.ticksPerSecond };  // desired frame Time
    const uint64_t spiralOfDeath{ FRAME_DELTA_TIME * 4 };
    std::array< uint64_t, 16 > avgFreqs;
    avgFreqs.fill ( FRAME_DELTA_TIME );

    frd.framePreviousTime = counter.counter ( );
    while ( game.gameRunning ( ) ) {
      ++frd.frameTick;
      frd.frameCurrentTime = counter.counter ( );
      //
      static auto frameLastTime = frd.frameCurrentTime;
      if ( frd.frameCurrentTime - frameLastTime >= counter.frequency ( ) ) {
        frd.fps       = frd.frameTick;
        frd.frameTick = 0;
        frameLastTime = counter.counter ( );
      }
      //
      frd.frameDeltaTime    = std::clamp ( frd.frameCurrentTime - frd.framePreviousTime, 0uL, spiralOfDeath );
      frd.frameDelta        = static_cast< float > ( frd.frameDeltaTime ) / counter.frequency ( );
      frd.framePreviousTime = frd.frameCurrentTime;
      //
      std::ranges::rotate ( avgFreqs.begin ( ), avgFreqs.begin ( ) + 1, avgFreqs.end ( ) );
      avgFreqs[ avgFreqs.size ( ) - 1 ] = frd.frameDeltaTime;
      frd.frameDeltaAvgTime = std::accumulate ( avgFreqs.begin ( ), avgFreqs.end ( ), 0UL ) / avgFreqs.size ( );
      frd.frameAccumulator += frd.frameDeltaAvgTime;
      if ( frd.frameAccumulator >= spiralOfDeath ) {
        frd.frameAccumulator  = 0;
        frd.frameDeltaAvgTime = FRAME_DELTA_TIME;
      }
      game.gameEvent ( );
      frd.updateTick++;

      game.gameUpdate ( frd.frameDelta );
      while ( frd.frameAccumulator >= FRAME_DELTA_TIME ) {
        frd.updateFixedTick++;
        frd.frameAccumulator -= FRAME_DELTA_TIME;
        // float interpolatedValue = static_cast<float>( frd.frameAccumulator) / FRAME_DELTA_TIME ;
        game.gameUpdateFixed ( frd.fixedDelta );
      }
      game.gameRender ( frd );
    }
  }
};

}  // namespace gd