#pragma once

#include <map>
#include <memory>

namespace gd {

template < class T >
class Singleton {
 protected:
  Singleton ( )                             = default;
  ~Singleton ( )                            = default;
  Singleton ( const Singleton& )            = delete;
  Singleton& operator= ( const Singleton& ) = delete;

 public:
  static T& get ( ) {
    static T instance;
    return instance;
  }
};

template < typename Key, typename Value >
class Multiton {
 public:
  static std::shared_ptr< Value > get ( const Key& key ) {
    if ( const auto it = _instances.find ( key ); it != _instances.end ( ) ) {
      return it->second;
    }
    return _instances[ key ] = std::make_shared< Value > ( );
  }

 private:
  static std::map< Key, std::shared_ptr< Value > > _instances;
};

template < typename Key, typename Value >
std::map< Key, std::shared_ptr< Value > > Multiton< Key, Value >::_instances;

}  // namespace gd