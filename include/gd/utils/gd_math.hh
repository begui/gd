#pragma once
#include <cmath>
#include <cstdint>
#include <random>

namespace gd::math {
constexpr bool in_range ( auto low, auto high, auto val ) { return ( val >= low && val <= high ); }

template <typename T>
inline T random(T low, T high) {
    static std::random_device rd;
    static std::mt19937 gen(rd());

    if constexpr (std::is_integral_v<T>) {
        std::uniform_int_distribution<T> dist(low, high);
        return dist(gen);
    } else {
        std::uniform_real_distribution<T> dist(low, high);
        return dist(gen);
    }
}

class PseudoRandomGenerator {
public:
    enum class Algorithm {
        XORShift,
        LCG
    };

    // Constructor that takes an initial seed and algorithm
    constexpr PseudoRandomGenerator(uint32_t initial_seed, Algorithm algo = Algorithm::LCG)
        : seed(initial_seed), algorithm(algo) {}

    // Linear Congruential Generator (LCG)
    static constexpr uint32_t lcg(uint32_t seed) {
        return (seed * 1664525u + 1013904223u) % 4294967296u;
    }

    // XORShift algorithm for pseudo-random number generation
    static constexpr uint32_t xorshift(uint32_t seed) {
        seed ^= seed << 13;
        seed ^= seed >> 17;
        seed ^= seed << 5;
        return seed;
    }

    // Generate the next random number in the sequence (no need to call .get())
    constexpr uint32_t next_n(unsigned int n = 1) const {
        PseudoRandomGenerator gen = *this;
        for (unsigned int i = 0; i < n; ++i) {
            gen = gen.next();
        }
        return gen.seed;  // Return the next random number in the sequence
    }

private:
    // Generate the next generator instance (returns a new generator with updated seed)
    constexpr PseudoRandomGenerator next() const {
        uint32_t new_seed;
        switch (algorithm) {
            case Algorithm::XORShift:
                new_seed = xorshift(seed);
                break;
            case Algorithm::LCG:
                new_seed = lcg(seed);
                break;
        }
        return PseudoRandomGenerator(new_seed, algorithm);  // Return a new generator with the updated seed
    }

    uint32_t seed;
    Algorithm algorithm;
};


}  // namespace gd::math