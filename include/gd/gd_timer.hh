#pragma once

#include <cstdint>

namespace gd {
class Timer final {
 public:
  uint64_t counter ( ) const noexcept;
  uint64_t frequency ( ) const noexcept;
  uint64_t tick ( ) const noexcept;

};

}  // namespace gd