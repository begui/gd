#pragma once

#include <cstdint>

// static_assert ( std::is_same< entt::id_type, gd::id_t > ( ), "Must be of uint32_t" );
namespace gd {
using id_t  = uint32_t;
using cid_t = const id_t;

}  // namespace gd
