#pragma once

#include "gd/utils/gd_pimpl.hh"

namespace gd {
class GDApp;

class Audio final {
  friend class GDApp;

 public:
  enum class Type
  {
    MUSIC,
    SFX
  };

 private:
  class Impl;
  gd::Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

  Audio ( ) noexcept;
  virtual ~Audio ( );
  Audio ( const Audio & ) = delete;
  Audio &operator= ( const Audio & ) = delete;
  Audio ( Audio && )                 = delete;
  Audio &operator= ( const Audio && ) = delete;

 public:
  bool load ( Audio::Type audioType, int32_t id, const char *file );
  void unload ( Audio::Type audioType, int32_t id );
  void play ( Audio::Type audioType, int32_t id, int32_t msFadeIn = 0, bool repeat = false ) const;
  void stop ( Audio::Type audioType, int32_t id, int32_t msFadeOut = 0 ) const;
  void pause ( Audio::Type audioType, int32_t id, bool pause ) const;
  void volume ( Audio::Type audioType, int32_t id, int32_t vol ) const;
  void volumeUp ( Audio::Type audioType, int32_t id, int32_t vol = 4 ) const;
  void volumeDown ( Audio::Type audioType, int32_t id, int32_t vol = -4 ) const;
  //
  void setSfxChannels ( int32_t channel ) const;
  int32_t getSfxChannels ( ) const;
  void freeSfxChannes ( ) const;
};
}  // namespace gd
