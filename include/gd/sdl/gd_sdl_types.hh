#pragma once

#include <cmath>
#include <cstdint>
#include <gd/sdl/gd_sdl_core.hh>
#include <optional>

namespace sdl {

enum class FlipType {
  None       = SDL_FLIP_NONE,  //
  Horizontal = SDL_FLIP_HORIZONTAL,
  Vertical   = SDL_FLIP_VERTICAL
};

template < typename T >
struct Point : public std::conditional_t< std::is_floating_point_v< T >, SDL_FPoint, SDL_Point > {
  static_assert ( std::is_same_v< int32_t, T > || std::is_same_v< float, T >, "T must inherit from int or float " );

 public:
  Point ( T xx = 0, T yy = 0 ) : std::conditional_t< std::is_floating_point_v< T >, SDL_FPoint, SDL_Point >{ xx, yy } {}
  bool operator== ( const Point &other ) const;
  bool operator< ( const Point &other ) const;
  void operator*= ( const Point &other );
  void operator/= ( const Point &other );
};

template <typename T>
struct Rect : public std::conditional_t<std::is_floating_point_v<T>, SDL_FRect, SDL_Rect> {
    static_assert(std::is_same_v<T, int32_t> || std::is_same_v<T, float>,
                  "T must be int32_t or float");

  // 
  template < typename U >
  static constexpr bool is_assignment_of_size4 ( ) {
    return std::is_same_v< U, std::initializer_list< T > > ||
           ( std::is_same_v< U, std::array< T, 4 > > && ( U{ }.size ( ) == 4 ) );
  }

 public:
  Rect ( T xx = 0, T yy = 0, T ww = 0, T hh = 0 )
      : std::conditional_t< std::is_floating_point_v< T >, SDL_FRect, SDL_Rect >{  xx, yy, ww, hh  } {}

  //
  bool operator== ( const Rect< T > &other ) const {
    return ( this->x == other.x ) && ( this->y == other.y ) && ( this->w == other.w ) && ( this->h == other.h );
  }

  template <typename U>
std::enable_if_t<is_assignment_of_size4<U>(), Rect<T>&> operator=(const U& other) {
    auto it = other.begin();
    this->x = *it++;
    this->y = *it++;
    this->w = *it++;
    this->h = *it++;
    return *this;
}


  bool isEmpty ( ) const noexcept;
  bool pointInRect ( const Point< T > &point ) const noexcept;
  bool hasIntersection ( const Rect< T > &other ) const noexcept;

    // Conversion functions: toFloat and toInt
    template <typename U = T>
    auto toFloat() const -> typename std::enable_if_t<std::is_same_v<U, int32_t>, Rect<float>> {
        return Rect<float>{static_cast<float>(this->x), static_cast<float>(this->y),
                           static_cast<float>(this->w), static_cast<float>(this->h)};
    }

    template <typename U = T>
    auto toInt() const -> typename std::enable_if_t<std::is_same_v<U, float>, Rect<int32_t>> {
        return Rect<int32_t>{static_cast<int32_t>(std::round(this->x)),
                             static_cast<int32_t>(std::round(this->y)),
                             static_cast<int32_t>(std::round(this->w)),
                             static_cast<int32_t>(std::round(this->h))};
    }

};

struct Rotation {
  FlipType flip{ FlipType::None };
  double angle{ 0.0 };
  std::optional< Point< float > > angleCenter = std::nullopt;
};

struct Color : public ::SDL_Color {
 public:
  Color ( uint8_t r = 0, uint8_t g = 0, uint8_t b = 0, uint8_t a = SDL_ALPHA_OPAQUE );

  void red ( uint8_t r );
  void green ( uint8_t g );
  void blue ( uint8_t b );
  void alpha ( uint8_t a );
  void rgba ( uint8_t r, uint8_t g, uint8_t b, uint8_t a );
  uint8_t red ( ) const;
  uint8_t green ( ) const;
  uint8_t blue ( ) const;
  uint8_t alpha ( ) const;

  explicit operator uint32_t ( ) const;
  bool operator== ( const Color &other ) const;

  static const Color RED;
  static const Color GREEN;
  static const Color BLUE;
  static const Color BLACK;
  static const Color WHITE;
};

struct Colorf : public ::SDL_FColor {
 public:
  Colorf ( float r = 0.f, float g = 0.f, float b = 0.f, float a = SDL_ALPHA_OPAQUE );

  void red ( float r );
  void green ( float g );
  void blue ( float b );
  void alpha ( float a );
  void rgba ( float r, float g, float b, float a );
  float red ( ) const;
  float green ( ) const;
  float blue ( ) const;
  float alpha ( ) const;

  explicit operator uint32_t ( ) const;
  bool operator== ( const Colorf &other ) const;

  static const Colorf RED;
  static const Colorf GREEN;
  static const Colorf BLUE;
  static const Colorf BLACK;
  static const Colorf WHITE;
};

struct Vertex : public ::SDL_Vertex {
  Vertex ( const SDL_FPoint &pos, const SDL_FColor &col, const SDL_FPoint &texCoor = { 1.0f, 1.0f } )
      : SDL_Vertex{ pos, col, texCoor } {}
};

template < typename T >
concept ColorType = std::same_as< T, sdl::Color > || std::same_as< T, sdl::Colorf >;

using VertexOpt_t   = std::optional< Vertex >;
using Pointi32_t    = Point< int32_t >;
using Pointi32Opt_t = std::optional< Pointi32_t >;
using Pointf_t      = Point< float >;
using PointfOpt_t   = std::optional< Pointf_t >;
using Recti32_t     = Rect< int32_t >;
using Recti32Opt_t  = std::optional< Recti32_t >;
using Rectf_t       = Rect< float >;
using RectfOpt_t    = std::optional< Rectf_t >;
using RotationOpt_t = std::optional< Rotation >;

}  // namespace sdl
