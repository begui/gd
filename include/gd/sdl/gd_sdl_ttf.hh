#pragma once

#if GD_USE_SDL_TTF
#include <SDL3_ttf/SDL_ttf.h>

namespace  sdl::ttf {
inline constexpr const char * SubSystem = "SDL_TTF";
}


#endif
