#pragma once

#include "gd/sdl/gd_sdl_types.hh"
#include "gd/utils/gd_enum.hh"

namespace sdl {

enum class WindowFlagType : int64_t {
  // test
  Default= 0x00000000,
  // full screen (0x00000001)
  Fullscreen = SDL_WINDOW_FULLSCREEN,
  // opengl window (0x00000002)
  Opengl = SDL_WINDOW_OPENGL,
  // window is visible
  Occluded = SDL_WINDOW_OCCLUDED,
  // window is not visible
  Hidden = SDL_WINDOW_HIDDEN,
  // no decorations
  Borderless = SDL_WINDOW_BORDERLESS,
  // allows to be resizable
  Resizeable = SDL_WINDOW_RESIZABLE,
  // window is minimized
  Minimized = SDL_WINDOW_MINIMIZED,
  // window is maximized
  Maximized = SDL_WINDOW_MAXIMIZED,
  // window has mouse focus 
  Mousegrabbed = SDL_WINDOW_MOUSE_GRABBED,
  // window has input focus
  InputFocus = SDL_WINDOW_INPUT_FOCUS,
  // window has mouse focus
  MouseFocus = SDL_WINDOW_MOUSE_FOCUS,
  // window now created by SDL
  External = SDL_WINDOW_EXTERNAL,
  // high dpi if supported
  Highdpi = SDL_WINDOW_HIGH_PIXEL_DENSITY,
  //
  Mousecapture = SDL_WINDOW_MOUSE_CAPTURE,
  // Window will always be on top
  AlwaysOnTop = SDL_WINDOW_ALWAYS_ON_TOP,
  // window should be treated as a utility window, not showing in the task bar and window list
  Utility = SDL_WINDOW_UTILITY,
  // window should be treated as a tooltip and must be created using SDL_CreatePopupWindow()
  Tooltip = SDL_WINDOW_TOOLTIP, 
  // window should be treated as a popup menu and must be created using SDL_CreatePopupWindow()
  Popup = SDL_WINDOW_POPUP_MENU,
  // window has grabbed keyboard input
  Keyboardgrabbed = SDL_WINDOW_KEYBOARD_GRABBED,
  // window usable for Vulkan surface 
  Vulkan =  SDL_WINDOW_VULKAN,
  // window usable for Metal view
  Metal =  SDL_WINDOW_METAL,
  // window with transparent buffer
  Transparent = SDL_WINDOW_TRANSPARENT,
  //window should not be focusable
  Notfocus =  SDL_WINDOW_NOT_FOCUSABLE
};

template < typename... T>
requires  gd::typetrait_util::are_same< WindowFlagType, T... >::value
static int32_t window_flags ( WindowFlagType head = WindowFlagType::Default, T... tail ) noexcept {
  return  gd::enum_util::enum_class_or ( head, tail... );
}


class Window final {
 public:
  // Window ( ) noexcept = default;
  Window ( int32_t width, int32_t height,
           int32_t flags = static_cast< std::underlying_type_t< WindowFlagType > > ( WindowFlagType::Default) ) noexcept;
  Window ( const Window & )             = delete;
  Window &operator= ( const Window & )  = delete;
  Window ( Window && )                  = delete;
  Window &operator= ( const Window && ) = delete;
  virtual ~Window ( )                   = default;

  explicit( false ) operator SDL_Window * ( ) { return window_.get ( ); }
  explicit( false ) operator SDL_Window * ( ) const { return window_.get ( ); }


  bool init ( int32_t width, int32_t height, int32_t flags ) noexcept;
  bool initialized ( ) const noexcept;

  std::tuple< int32_t, int32_t > size ( ) const noexcept;
  std::tuple< float, float> sizef ( ) const noexcept;
  std::tuple< int32_t, int32_t > position ( ) const noexcept;

  uint32_t pixelFormat ( ) const noexcept;
  Window &title ( const std::string &title ) noexcept;
  Window &fullscreen ( bool flag ) noexcept;
  Window &center ( ) noexcept;
  Window &position ( int x, int y ) noexcept;
  Window &opacity ( float op ) noexcept;
  Window &show ( ) noexcept;
  Window &flip ( ) noexcept;
  Window &hide ( ) noexcept;
  Window &raise ( ) noexcept;
  Window &maximize ( ) noexcept;
  Window &minimize ( ) noexcept;
  Window &restore ( ) noexcept;
  static std::tuple< int32_t, int32_t > getAspectRatio ( int32_t width, int32_t height );

  
 private:
  sdl::WindowUniqPtr window_{ nullptr };
};




}  
