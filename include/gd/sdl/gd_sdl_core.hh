#pragma once

#include <SDL3/SDL.h>
#include "SDL3/SDL_gamepad.h"

#ifdef None
#undef None
#endif

#include <memory>
#include <string>

#include "gd/utils/gd_enum.hh"
#include "gd/utils/gd_type_traits.hh"
namespace sdl {
inline constexpr const char *SubSystem = "SDL";

namespace clock {

inline uint64_t counter ( ) { return ::SDL_GetPerformanceCounter ( ); }
inline uint64_t frequency ( ) {
  const static auto frequency = ::SDL_GetPerformanceFrequency ( );
  return frequency;
}
inline uint64_t tick ( ) { return ::SDL_GetTicks ( ); }

}  // namespace clock

namespace log {

enum class CategoryType {
  Application = SDL_LOG_CATEGORY_APPLICATION,
  Error       = SDL_LOG_CATEGORY_ERROR,
  Assert      = SDL_LOG_CATEGORY_ASSERT,
  System      = SDL_LOG_CATEGORY_SYSTEM,
  Audio       = SDL_LOG_CATEGORY_AUDIO,
  Video       = SDL_LOG_CATEGORY_VIDEO,
  Render      = SDL_LOG_CATEGORY_RENDER,
  Input       = SDL_LOG_CATEGORY_INPUT,
  Test        = SDL_LOG_CATEGORY_TEST,
  Custom      = SDL_LOG_CATEGORY_CUSTOM
};

enum class PriorityType : std::underlying_type_t< SDL_LogPriority > {
  Verbose     = SDL_LOG_PRIORITY_VERBOSE,
  Debug       = SDL_LOG_PRIORITY_DEBUG,
  Information = SDL_LOG_PRIORITY_INFO,
  Warning     = SDL_LOG_PRIORITY_WARN,
  Error       = SDL_LOG_PRIORITY_ERROR,
  Critical    = SDL_LOG_PRIORITY_CRITICAL
};

inline void set_priority ( const CategoryType cat, const PriorityType pri ) noexcept {
  SDL_SetLogPriority ( static_cast< std::underlying_type_t< CategoryType > > ( cat ),
                       static_cast< SDL_LogPriority > ( pri ) );
}
inline PriorityType get_priority ( const CategoryType cat ) noexcept {
  return static_cast< PriorityType > (
      SDL_GetLogPriority ( static_cast< std::underlying_type_t< CategoryType > > ( cat ) ) );
}
inline void reset_all_priorities ( ) noexcept { SDL_ResetLogPriorities ( ); }

template < typename... Args >
inline void log ( const char *fmt, Args &&...args ) noexcept {
  SDL_Log ( fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void verbose ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogVerbose ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt,
                   std::forward< Args > ( args )... );
}
template < typename... Args >
inline void debug ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogDebug ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void info ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogInfo ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void warn ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogWarn ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void error ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogError ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void critical ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogCritical ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt,
                    std::forward< Args > ( args )... );
}
inline void log ( const char *str ) noexcept { log ( "%s", str ); }
inline void verbose ( const CategoryType cat, const char *str ) noexcept { verbose ( cat, "GD: %s", str ); }
inline void debug ( const CategoryType cat, const char *str ) noexcept { debug ( cat, "GD: %s", str ); }
inline void info ( const CategoryType cat, const char *str ) noexcept { info ( cat, "GD: %s", str ); }
inline void warn ( const CategoryType cat, const char *str ) noexcept { warn ( cat, "GD: %s", str ); }
inline void error ( const CategoryType cat, const char *str ) noexcept { error ( cat, "GD: %s\n", str ); }
inline void critical ( const CategoryType cat, const char *str ) noexcept { critical ( cat, "GD: %s", str ); }

inline bool is_debug ( ) { return log::PriorityType::Debug == log::get_priority ( log::CategoryType::Application ); }

void log_system_info ( ) noexcept;

}  // namespace log

template < typename Ptr, void del_func ( Ptr * ) >
struct SDLDeleter {
  void operator( ) ( Ptr *ptr ) const noexcept {
    if ( ptr ) {
      if ( log::is_debug ( ) ) {
        std::string buf ( "Freeing " );
        buf.append ( typeid ( ptr ).name ( ) );
        log::debug ( log::CategoryType::Application, buf.c_str ( ) );
      }
      del_func ( ptr );
    }
  }
};

struct GLContextDeleter {
  void operator( ) ( SDL_GLContext *ptr ) const noexcept {
    if ( ptr ) {
      if ( log::is_debug ( ) ) {
        std::string buf ( "Freeing " );
        buf.append ( typeid ( ptr ).name ( ) );
        log::debug ( log::CategoryType::Application, buf.c_str ( ) );
      }
      SDL_GL_DestroyContext ( *ptr );
    }
  }
};

struct RWDeleter {
  void operator( ) ( SDL_IOStream *ptr ) const noexcept {
    if ( ptr )
      SDL_CloseIO ( ptr );
  }
};

namespace internal {
template < typename T, void del_func ( T * ) >
using sdl_ptr = std::unique_ptr< T, SDLDeleter< T, del_func > >;
}

using WindowUniqPtr    = internal::sdl_ptr< SDL_Window, &SDL_DestroyWindow >;
using RendererUniqPtr  = internal::sdl_ptr< SDL_Renderer, &SDL_DestroyRenderer >;
using GLContextUniqPtr = std::unique_ptr< SDL_GLContext, GLContextDeleter >;
using TextureUniqPtr   = internal::sdl_ptr< SDL_Texture, &SDL_DestroyTexture >;
using SurfaceUniqPtr   = internal::sdl_ptr< SDL_Surface, &SDL_DestroySurface >;
using GamePadUniPtr    = internal::sdl_ptr<SDL_Gamepad, &SDL_CloseGamepad>;

enum class SubsystemType {
  //
  Core = 1 << 0,
  //
  Ttf = 1 << 1,
  //
  Image = 1 << 2,
  //
  Audio = 1 << 3
};

enum class InitType : uint32_t {
  Default = 0x00u,
  //
  Audio = SDL_INIT_AUDIO,
  //
  Video = SDL_INIT_VIDEO,
  //
  Joystick = SDL_INIT_JOYSTICK,
  //
  Haptic = SDL_INIT_HAPTIC,
  //
  Gamepad = SDL_INIT_GAMEPAD,
  //
  Events = SDL_INIT_EVENTS,
  //
  Sensor = SDL_INIT_SENSOR,
  //
  Camera = SDL_INIT_CAMERA
};

bool init ( uint32_t flags ) noexcept;
template < typename... T,
           typename = std::enable_if_t< gd::typetrait_util::are_same< sdl::InitType, T... >::value, void > >
int32_t init ( InitType flag0 = InitType::Default, T... flagN ) noexcept {
  const uint32_t flags = gd::enum_util::enum_class_or ( flag0, flagN... );
  return init ( flags );
}
auto get_version ( SubsystemType subSystem ) noexcept;
///
// system::cpu
///
namespace system {
namespace cpu {
inline auto count ( ) noexcept { return ::SDL_GetNumLogicalCPUCores( ); }
inline auto has_altivec ( ) noexcept { return ::SDL_HasAltiVec ( ); }
inline auto has_avx ( ) noexcept { return ::SDL_HasAVX ( ); }
inline auto has_avx2 ( ) noexcept { return ::SDL_HasAVX2 ( ); }
inline auto has_mmx ( ) noexcept { return ::SDL_HasMMX ( ); }
inline auto has_sse ( ) noexcept { return ::SDL_HasSSE ( ); }
inline auto has_sse2 ( ) noexcept { return ::SDL_HasSSE2 ( ); }
inline auto has_sse3 ( ) noexcept { return ::SDL_HasSSE3 ( ); }
inline auto has_sse41 ( ) noexcept { return ::SDL_HasSSE41 ( ); }
inline auto has_sse42 ( ) noexcept { return ::SDL_HasSSE42 ( ); }
}  // namespace cpu

inline auto platform ( ) noexcept { return ::SDL_GetPlatform ( ); }
inline auto ram ( ) noexcept { return ::SDL_GetSystemRAM ( ); }
inline auto ram_gb ( ) noexcept { return ram ( ) / 1'000; }
inline auto video_driver ( int index = -1 ) noexcept {
  if ( const auto numVideoDrivers = ::SDL_GetNumVideoDrivers ( ); index < 0 || index > numVideoDrivers ) {
    return ::SDL_GetCurrentVideoDriver ( );
  }
  return ::SDL_GetVideoDriver ( index );
}
}  // namespace system

struct KB {
  static bool keyboard ( const SDL_Scancode scancode ) noexcept;
  static bool one ( ) noexcept;
  static bool two ( ) noexcept;
  static bool three ( ) noexcept;
  static bool four ( ) noexcept;
  static bool five ( ) noexcept;
  static bool six ( ) noexcept;
  static bool seven ( ) noexcept;
  static bool eight ( ) noexcept;
  static bool nine ( ) noexcept;
  static bool zero ( ) noexcept;
  static bool up ( ) noexcept;
  static bool down ( ) noexcept;
  static bool left ( ) noexcept;
  static bool right ( ) noexcept;
  static bool space ( ) noexcept;
  static bool w ( ) noexcept;
  static bool a ( ) noexcept;
  static bool d ( ) noexcept;
  static bool s ( ) noexcept;
};

enum class MouseStateType {
  // Coordinates based on the resolution of the window and not its logical size
  None,
  // The direction of the mouse (pos / neg numbers)
  Relative,
  // Global mouse coordinate based on users screen resolution
  Global
};
struct MOUSE {
  static int32_t mouse ( const MouseStateType type = MouseStateType::None ) noexcept;
  static bool left ( ) noexcept;
  static bool right ( ) noexcept;
  static bool middle ( ) noexcept;
  static auto position ( const MouseStateType type = MouseStateType::None ) noexcept -> std::tuple< float, float >;
  // TODO: Do we need this?
  static bool left_relative ( ) noexcept;
  static bool right_relative ( ) noexcept;
  static bool middle_relative ( ) noexcept;
  static auto position_relative ( ) noexcept -> std::tuple< float, float >;
  // TODO: Do we need this?
  static bool left_global ( ) noexcept;
  static bool right_global ( ) noexcept;
  static bool middle_global ( ) noexcept;
  static auto position_global ( ) noexcept -> std::tuple< float, float >;
  //
  // While in Relative Mode, the cursor is hidden.
  //
  static void enable_mouse_relative ( SDL_Window *window, const bool enable ) noexcept;
  static void enable_mouse_grab ( SDL_Window *window, const bool enable ) noexcept;
  static void enable_mouse_cursor ( const bool enable ) noexcept;
};

}  // namespace sdl
