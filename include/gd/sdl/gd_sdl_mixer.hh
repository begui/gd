#pragma once

#include "gd/sdl/gd_sdl_core.hh"

#if GD_USE_SDL_MIXER
#include <SDL3_mixer/SDL_mixer.h>

namespace  sdl::mixer {
inline constexpr const char * SubSystem = "SDL_MIXER";
enum class MixerType {
  FLAC = MIX_INIT_FLAC,  //
  MOD  = MIX_INIT_MOD,
  MP3  = MIX_INIT_MP3,
  OGG  = MIX_INIT_OGG,
  MID  = MIX_INIT_MID,
  OPUS = MIX_INIT_OPUS
};

using MixChunkUniqPtr = internal::sdl_ptr< Mix_Chunk, &Mix_FreeChunk >;
using MixMusicUniqPtr = internal::sdl_ptr< Mix_Music, &Mix_FreeMusic >;


}
#endif
