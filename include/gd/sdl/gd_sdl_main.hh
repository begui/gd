#pragma once

#include <SDL3/SDL.h>

#include <memory>

namespace sdl {
class SDLAppMain {
 public:
  virtual ~SDLAppMain ( ) = default;

 private:
  virtual bool onInit ( const int argc, char** argv ) noexcept        = 0;
  virtual void onExit ( ) noexcept                                    = 0;
  virtual void onUpdate ( float fixedDelta ) noexcept                 = 0;
  virtual void onRender ( float delta, float interpolation ) noexcept = 0;
  virtual void onEvent( SDL_Event& event ) noexcept            = 0;
  ////
  // Events
  ////
  // Display
  virtual void onDisplay ( const SDL_DisplayEvent& ) noexcept {}
  virtual void onWindow ( const SDL_WindowEvent& ) noexcept {}
  // Keyboard
  virtual void onKeyboardDevice ( const SDL_KeyboardDeviceEvent&, const bool ) noexcept {}
  virtual void onKeyboard ( const SDL_KeyboardEvent&, const bool ) noexcept {}
  // Mouse
  virtual void onMouseDevice ( const SDL_MouseDeviceEvent&, const bool ) noexcept {}
  virtual void onMouseMotion ( const SDL_MouseMotionEvent& ) noexcept {}
  virtual void onMouseButton ( const SDL_MouseButtonEvent&, const bool ) noexcept {}
  virtual void onMouseWheel ( const SDL_MouseWheelEvent& ) noexcept {}
  // Gamepad
  virtual void onGamepadDevice ( const SDL_GamepadDeviceEvent& ) noexcept {}
  virtual void onGamepadAxis ( const SDL_GamepadAxisEvent& ) noexcept {}
  virtual void onGamepadButton ( const SDL_GamepadButtonEvent& ) noexcept {}
  virtual void onGamepadTouch ( const SDL_GamepadTouchpadEvent& ) noexcept {}
  virtual void onGamepadSensor ( const SDL_GamepadSensorEvent& ) noexcept {}
  // Audio
  virtual void onAudioDevice ( const SDL_AudioDeviceEvent& ) noexcept {}
  // Touch
  virtual void onTouchFinger ( const SDL_TouchFingerEvent& ) noexcept {}

  //
  bool appInit ( const int argc, char** argv ) noexcept;
  bool appIterate ( ) noexcept;
  bool appEvent ( SDL_Event& event ) noexcept;
  void appQuit ( ) noexcept;
  //
  // Our kickstart function. Implement this function to kickstart your application
  static std::unique_ptr< sdl::SDLAppMain > init ( int argc, char** argv );

 private:
  friend SDL_AppResult SDL_AppInit_Impl ( void** appstate, int argc, char** argv ) noexcept;
  friend SDL_AppResult SDL_AppIterate_Impl ( void* appstate ) noexcept;
  friend SDL_AppResult SDL_AppEvent_Impl ( void* appstate, SDL_Event* event ) noexcept;
  friend void SDL_AppQuit_Impl ( void* appstate, SDL_AppResult result ) noexcept;
};

}  // namespace sdl
