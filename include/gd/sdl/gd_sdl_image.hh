#pragma once

#if GD_USE_SDL_IMAGE

#include <SDL3_image/SDL_image.h>

namespace sdl::image {

inline constexpr const char* SubSystem = "SDL_IMAGE";

}  // namespace sdl::image

#endif