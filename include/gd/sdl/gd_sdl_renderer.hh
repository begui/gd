#pragma once

#include <functional>
#include <optional>
#include <vector>
#include <cstdlib>

#include "gd/gd_types.hh"
#include "gd/sdl/gd_sdl_types.hh"
#include "gd/sdl/gd_sdl_window.hh"
#include "gd/utils/gd_pimpl.hh"
#include "gd/utils/gd_type_traits.hh"

namespace sdl {

enum class BlendmodeType : uint32_t {
  None    = SDL_BLENDMODE_NONE,
  Blend   = SDL_BLENDMODE_BLEND,
  BlendPM = SDL_BLENDMODE_BLEND_PREMULTIPLIED,
  Add     = SDL_BLENDMODE_ADD,
  AddPM   = SDL_BLENDMODE_ADD_PREMULTIPLIED,
  Mod     = SDL_BLENDMODE_MOD,
  Mul     = SDL_BLENDMODE_MUL,
  Invalid = SDL_BLENDMODE_INVALID
};


class Renderer final {
  template < gd::FloatOrUInt8 T >
  friend T get_alpha ( const Renderer &renderer, gd::cid_t id ) noexcept;

  template < gd::FloatOrUInt8 T >
  friend void set_alpha ( Renderer &renderer, gd::cid_t id, const T alpha ) noexcept;

  template < typename... Args >
  friend bool load_texture(Renderer &renderer, gd::cid_t id, Args &&...args ) noexcept; 

 public:
  enum class PresentationType {
    None         = SDL_LOGICAL_PRESENTATION_DISABLED,
    Strech       = SDL_LOGICAL_PRESENTATION_STRETCH,
    Letterbox    = SDL_LOGICAL_PRESENTATION_LETTERBOX,
    Overscan     = SDL_LOGICAL_PRESENTATION_OVERSCAN,
    IntegerScale = SDL_LOGICAL_PRESENTATION_INTEGER_SCALE
  };
  // static constexpr std::array<const char*, 5> PresentationTypeNames = {
   static constexpr const char* PresentationTypeNames[] {
        "None", 
        "Stretch", 
        "Letterbox", 
        "Overscan", 
        "Integer Scale"
  };

  enum class ScaleModeType {
    Nearest = SDL_SCALEMODE_NEAREST,
    Linear  = SDL_SCALEMODE_LINEAR,
  };

 public:
  explicit Renderer ( const Window &window );
  Renderer ( )                              = delete;
  Renderer ( const Renderer & )             = delete;
  Renderer &operator= ( const Renderer & )  = delete;
  Renderer ( Renderer && )                  = delete;
  Renderer &operator= ( const Renderer && ) = delete;
  ~Renderer ( );
  //
  explicit ( false ) operator SDL_Renderer * ( );
  explicit ( false ) operator SDL_Renderer * ( ) const;
  //
  bool init ( const Window &window, const char *name = nullptr ) noexcept;
  bool initialized ( ) const noexcept;
  //
  std::tuple< int32_t, int32_t > size ( ) const noexcept;
  std::tuple< float, float > sizef ( ) const noexcept;
  //
  sdl::Recti32_t getViewPort ( ) const noexcept;
  void setViewPort ( const sdl::Recti32Opt_t &rect = std::nullopt) noexcept;
  //
  sdl::Rectf_t getLogicalRect() const noexcept;
  std::tuple<int32_t, int32_t> getLogicalSize() const noexcept;
  void setLogicalSize ( const int32_t width, const int32_t height, PresentationType type = PresentationType::Letterbox) const noexcept;
  //
  std::tuple<float, float> getScale() const noexcept;
  void setScale ( float scaleX, float scaleY ) const noexcept;
  //
  BlendmodeType getBlendMode ( ) const noexcept;
  void setBlendMode ( BlendmodeType blendmode ) const noexcept;
 // 
  std::tuple< float, float > logicalMousePosition ( ) const noexcept;
  void setVSync ( bool enable ) noexcept;
  void clear ( ) const noexcept;
  void swap ( ) const noexcept;
  // Texturing
  //
  void textureBlendMode ( gd::cid_t id, const BlendmodeType mode ) noexcept;
  BlendmodeType textureBlendMode ( gd::cid_t id ) const noexcept;
  //
  void textureAlpha ( gd::cid_t id, const uint8_t alpha ) noexcept;
  uint8_t textureAlpha ( gd::cid_t id ) const noexcept;
  void textureAlphaf ( gd::cid_t id, const float alpha ) noexcept;
  float textureAlphaf ( gd::cid_t id ) const noexcept;
  void texturePixelFormat ( const SDL_PixelFormat format );
  const char *texturePixelFormatName ( ) const;
  std::tuple< float, float > textureSize ( gd::cid_t id ) const noexcept;
  //
  bool loadStaticTexture ( gd::cid_t id, const std::string &filename ) noexcept;
  bool loadTargetTexture ( gd::cid_t id, const std::int32_t w, const std::int32_t h,
                           const std::function< void ( ) >& func ) noexcept;
  //
  bool loadStreamTexture ( gd::cid_t id, const std::int32_t w, const std::int32_t h ) noexcept;
  bool updateStreamTexture ( gd::cid_t id, const std::function< void ( std::uint32_t *pixels ) >& func,
                             const sdl::Recti32_t &rect ) noexcept;

  void unloadTexture ( gd::cid_t id ) noexcept;

  size_t textureCacheSize ( ) const noexcept;
  void textureCacheClear ( ) noexcept;

  void drawTexture ( const gd::cid_t id,                                  //
                     const sdl::RectfOpt_t &src          = std::nullopt,  //
                     const sdl::RectfOpt_t &dst          = std::nullopt,  //
                     const float scale                  = 0.f,
                     const sdl::RotationOpt_t &rotation = std::nullopt ) const noexcept;

  // Drawing Primitivies
  template <ColorType COLORT>
  void setColor(const COLORT &color) const noexcept;
  template <ColorType COLORT>
  void drawBackground(const COLORT &color) const noexcept;
  template <ColorType COLORT>
  void drawPoint ( const sdl::Pointf_t &p1, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawPoints ( const std::vector< sdl::Pointf_t > &pts, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawLine ( const sdl::Pointf_t &p1, const sdl::Pointf_t &p2, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawLineH ( const float y, const float x1, const float x2, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawLineV ( const float x, const float y1, const float y2, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawLines ( const std::vector< sdl::Pointf_t > &pts, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawRect ( const sdl::Rectf_t &rect, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawRects ( const std::vector< sdl::Rectf_t > &rects, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawRectFill ( const sdl::Rectf_t &rect, const COLORT &color ) const noexcept;
  template <ColorType COLORT>
  void drawRectsFill ( const std::vector< sdl::Rectf_t > &rects, const COLORT &color ) const noexcept;
  //
  void drawGeometry ( const std::vector< sdl::Vertex > &v ) const noexcept;

 private:
  // Pimpl
  struct Impl;
  gd::Pimpl< Impl > impl_;

  Impl &impl ( );
  Impl &impl ( ) const;

};

}  // namespace sdl
