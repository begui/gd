#pragma once
#include <memory>

namespace gd {

using ByteDataUniqPtr = std::unique_ptr< uint8_t[] >;

class FileSystem final {
 public:
  static bool init ( const char *basePath ) noexcept;
  static bool isInit ( ) noexcept;
  static void destroy ( ) noexcept;

  static bool fileExists ( const char *file ) noexcept;
  static bool dirExists ( const char *dir ) noexcept;
  static bool mountDirectory ( const char *directory ) noexcept;
  static bool mountArchive ( const char *archive, const char *mountpoint ) noexcept;
  static bool unmount ( const char *archiveOrDir ) noexcept;
  static std::tuple< ByteDataUniqPtr, std::size_t > load ( const char *filePath ) noexcept;

 private:
  static bool mount ( const char *archiveOrDir, const char *mountpoint ) noexcept;
};

}  // namespace gd