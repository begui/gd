#pragma once

#include <entt/entt.hpp>

#include "gd/gd_types.hh"

#include "gd/sdl/gd_sdl_core.hh"
#include "gd/sdl/gd_sdl_main.hh"
#include "gd/sdl/gd_sdl_types.hh"
#include "gd/sdl/gd_sdl_renderer.hh"
#include "gd/sdl/gd_sdl_window.hh"
#if GD_USE_SDL_IMAGE
#include "gd/sdl/gd_sdl_image.hh"
#endif
#if GD_USE_SDL_MIXER 
#include "gd/sdl/gd_sdl_mixer.hh"
#endif
#if GD_USE_SDL_TTF
#include "gd/sdl/gd_sdl_ttf.hh"
#endif


#if GD_USE_IMGUI
#include "imgui.h"
#include "imgui_impl_sdl3.h"
#include "imgui_impl_sdlrenderer3.h"
#endif

namespace gd {

class SDLApp : public sdl::SDLAppMain {
 public:
  virtual ~SDLApp ( ) = default;

  class Scene {
    friend class SDLApp;

   public:
    explicit Scene ( SDLApp &app ) : app_ ( app ) {}
    virtual ~Scene ( )                             = default;
    virtual int32_t id ( ) const noexcept          = 0;
    virtual void onEnter ( ) noexcept              = 0;
    virtual void onExit ( ) noexcept               = 0;
    virtual void update ( float dt ) noexcept      = 0;
    virtual void updateFixed ( float dt ) noexcept = 0;
    virtual void render ( ) const noexcept         = 0;
    //
    SDLApp &app ( ) const noexcept { return app_; }

   private:
    SDLApp &app_;
  };  // End of class Scene

  template < typename TSceneInstance, typename... Args >
  SDLApp &addScene ( Args &&...args ) noexcept {
    static_assert ( std::is_base_of< Scene, TSceneInstance >::value, "SceneType must be derived from Scene" );
    auto scene         = std::make_shared< TSceneInstance > ( *this, std::forward< Args > ( args )... );
    auto sceneId       = scene->id ( );
    scenes_[ sceneId ] = scene;
    return *this;
  }

  using SceneSPtr = std::shared_ptr< Scene >;
  using SceneWPtr = std::weak_ptr< Scene >;

  class SceneTransition {
    friend class SDLApp;

   public:
    explicit SceneTransition ( SDLApp &app ) : app_ ( app ) {}
    virtual ~SceneTransition ( ) = default;

   protected:
    //
    virtual void onEnter ( ) noexcept = 0;
    virtual void onExit ( ) noexcept  = 0;
    virtual void update ( float dt )  = 0;
    virtual void render ( ) const     = 0;
    virtual bool isComplete ( ) const = 0;
    //
    void start ( SceneWPtr fromScene, SceneWPtr toScene ) {
      fromScene_       = fromScene;
      toScene_         = toScene;
      isTransitioning_ = true;
    }
    auto &app ( ) const noexcept { return app_; }
    auto getFromScene ( ) const noexcept { return fromScene_; }
    auto getToScene ( ) const noexcept { return toScene_; }
    auto isTransitioning ( ) const noexcept { return isTransitioning_; }
    void setTransition ( bool enable ) { isTransitioning_ = enable; }

   private:
    SDLApp &app_;
    SceneWPtr fromScene_;
    SceneWPtr toScene_;
    bool isTransitioning_{ false };
  };  // End of class SceneTransition

  using SceneTransitionSPtr = std::shared_ptr< SceneTransition >;
  using SceneTransitionWPtr = std::weak_ptr< SceneTransition >;

  class Options final {
    friend class GDApp;

   public:
    template < typename... T >
      requires std::conjunction_v< gd::typetrait_util::are_same< sdl::InitType, T >... >
    Options &init ( sdl::InitType flag0 = sdl::InitType::Default, T... flagN ) noexcept {
      initFlags_ = enum_util::enum_class_or ( flag0, flagN... );
      return *this;
    }

#if GD_USE_SDL_TTF
    Options &initTTF ( ) noexcept {
      ttfFlags_ = 0;
      return *this;
    }
#endif

#if GD_USE_SDL_MIXER
    template < typename... T >
      requires std::conjunction_v< gd::typetrait_util::are_same< sdl::mixer::MixerType, T >... >
    Options &initMixer ( sdl::mixer::MixerType flag0 = sdl::mixer::MixerType::OGG, T... flagN ) noexcept {
      audioFlags_ = gd::enum_util::enum_class_or ( flag0, flagN... );
      return *this;
    }
#endif

    template < typename... T >
      requires std::conjunction_v< gd::typetrait_util::are_same< sdl::WindowFlagType, T >... >
    Options &setWindowFlags ( sdl::WindowFlagType flag0 = sdl::WindowFlagType::Default, T... flagN ) noexcept {
      windowFlags_ = gd::enum_util::enum_class_or ( flag0, flagN... );
      return *this;
    }

    Options &setWindowSize ( int32_t width, int32_t height ) noexcept {
      windowWidth_  = width;
      windowHeight_ = height;
      return *this;
    }

    Options &setRendererSize ( int32_t width = 0, int32_t height = 0 ) noexcept {
      rendererWidth_  = width;
      rendererHeight_ = height;
      return *this;
    }

    Options &setRendererPresentation ( sdl::Renderer::PresentationType presentationType ) noexcept {
      presentation_ = presentationType;
      return *this;
    }

    Options &setTicksPerSecond ( const int32_t tickPerSec ) {
      this->ticksPerSecond_ = tickPerSec;
      return *this;
    }

    Options &setLogLevel ( sdl::log::PriorityType level ) noexcept {
      sdl::log::set_priority ( sdl::log::CategoryType::Application, level );
      return *this;
    }

    auto getInitFlags ( ) const noexcept { return initFlags_; }
#if GD_USE_SDL_IMAGE
    auto getImageInitFlags ( ) const noexcept { return imageFlags_; }
#endif
    auto getWindowFlags ( ) const noexcept { return windowFlags_; }
    auto getWindowWidth ( ) const noexcept { return windowWidth_; }
    auto getWindowHeight ( ) const noexcept { return windowHeight_; }
    auto getWindowSize ( ) const noexcept { return std::make_tuple ( getWindowWidth ( ), getWindowHeight ( ) ); }

    auto getRendererWidth ( ) const noexcept { return rendererWidth_; }
    auto getRendererHeight ( ) const noexcept { return rendererHeight_; }
    auto getRendererSize ( ) const noexcept { return std::make_tuple ( getRendererWidth ( ), getRendererHeight ( ) ); }
    auto getRendererPresentation ( ) const noexcept { return presentation_; }

#if GD_USE_SDL_MIXER
    auto getAudioFlags ( ) const { return audioFlags_; }
    auto getSfxChannels ( ) const { return sfxChannels_; }
#endif
    auto getTicksPerSecond ( ) const { return ticksPerSecond_; }

   private:
    uint32_t initFlags_{ gd::typetrait_util::to_type ( sdl::InitType::Default ) };
    uint32_t windowFlags_{ gd::typetrait_util::to_type ( sdl::WindowFlagType::Default ) };
    int32_t windowWidth_{ 320 };
    int32_t windowHeight_{ 240 };
    int32_t rendererWidth_{ 0 };
    int32_t rendererHeight_{ 0 };
    sdl::Renderer::PresentationType presentation_{ sdl::Renderer::PresentationType::None };

#if GD_USE_SDL_IMAGE
    int32_t imageFlags_{ -1 };
#endif
#if GD_USE_SDL_TTF
    int32_t ttfFlags_{ -1 };
#endif
#if GD_USE_SDL_MIXER
    int32_t audioFlags_{ -1 };
    int32_t sfxChannels_{ 16 };
#endif
    int32_t ticksPerSecond_{ 60 };
  };  // End of class Option

  virtual bool init ( const int argc, char *argv[] ) noexcept       = 0;
  virtual void destroy ( ) noexcept                                 = 0;
  virtual void update ( float delta ) noexcept                      = 0;
  virtual void render ( float delta, float interpolation ) noexcept = 0;

 public:
  Options options;

  struct Locator {
    using window   = entt::locator< sdl::Window >;
    using renderer = entt::locator< sdl::Renderer >;
    // using audio    = entt::locator< Audio >;
  };
  sdl::Window &Window ( ) const noexcept { return Locator::window::value ( ); }
  sdl::Renderer &Renderer ( ) const noexcept { return Locator::renderer::value ( ); }

 protected:
  void setTransitionTo ( std::shared_ptr< SceneTransition > transition ) noexcept;
  void changeSceneTo ( int32_t id, std::shared_ptr< SceneTransition > transition = nullptr ) noexcept;

 private:
  bool onInit ( const int argc, char *argv[] ) noexcept final override;
  void onExit ( ) noexcept final override;
  void onUpdate ( float fixedDelta ) noexcept final override;
  void onRender ( float delta, float interpolation ) noexcept final override;
  void onEvent( SDL_Event &event) noexcept final override;

  // Scene Managment
  std::unordered_map< gd::id_t, SceneSPtr > scenes_;
  SceneWPtr currentScene_;
  std::shared_ptr< SceneTransition > sceneTransition_;
};
}  // namespace gd
