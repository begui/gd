#include "gd/sdl/gd_sdl_core.hh"
#include "gd/sdl/gd_sdl_main.hh"

#if GD_USE_IMGUI
#include "imgui_impl_sdl3.h"
#endif

#define SDL_MAIN_USE_CALLBACKS 1  // Use the callbacks instead of main()
#include <SDL3/SDL_main.h>

namespace sdl {

bool SDLAppMain::appInit ( const int argc, char** argv ) noexcept {
  return onInit ( argc, argv );
}
bool SDLAppMain::appIterate ( ) noexcept {
  static const auto FIXED_TIME_STEP             = 1.f / ( 60.f );  // 60 FPS
  static const auto MAX_FIXED_UPDATES_PER_FRAME = 5;               // Base cap for fixed updates
  static uint64_t previousTime                  = sdl::clock::counter ( );
  static float accumulator                      = 0.0;
  static int32_t fixedUpdateCount               = 0;

  uint64_t currentTime = sdl::clock::counter ( );
  float deltaTime      = static_cast< float > ( currentTime - previousTime ) / sdl::clock::frequency ( );
  previousTime         = currentTime;
  //
  accumulator += deltaTime;
  fixedUpdateCount = 0;
  while ( accumulator >= FIXED_TIME_STEP ) {
    onUpdate ( FIXED_TIME_STEP );
    accumulator -= FIXED_TIME_STEP;
    fixedUpdateCount++;
  }
  if ( fixedUpdateCount == MAX_FIXED_UPDATES_PER_FRAME ) {
    accumulator = 0.0;
    sdl::log::warn ( sdl::log::CategoryType::Application, "Skipping remaining time to avoid spiral of death." );
  }

  const auto interpolation = accumulator / FIXED_TIME_STEP;
  onRender ( deltaTime, interpolation );

  return true;
}

bool SDLAppMain::appEvent ( SDL_Event& event ) noexcept {
#if GD_USE_IMGUI
  ImGui_ImplSDL3_ProcessEvent ( &event );
#endif
  onEvent(event);

  switch ( event.type ) {
    case SDL_EVENT_QUIT:
      return false;
      break;
    case SDL_EVENT_KEYBOARD_ADDED:
    case SDL_EVENT_KEYBOARD_REMOVED:
      onKeyboardDevice ( event.kdevice, event.type == SDL_EVENT_KEYBOARD_ADDED );
      break;
    case SDL_EVENT_KEY_DOWN:
    case SDL_EVENT_KEY_UP:
      if ( 0 == event.key.repeat ) {
        onKeyboard ( event.key, event.type == SDL_EVENT_KEY_DOWN );
      }
      break;
    case SDL_EVENT_MOUSE_ADDED:
    case SDL_EVENT_MOUSE_REMOVED:
      onMouseDevice ( event.mdevice, event.type == SDL_EVENT_MOUSE_ADDED );
      break;
    case SDL_EVENT_MOUSE_MOTION:
      onMouseMotion ( event.motion );
      break;
    case SDL_EVENT_MOUSE_BUTTON_UP:
    case SDL_EVENT_MOUSE_BUTTON_DOWN:
      onMouseButton ( event.button, event.type == SDL_EVENT_MOUSE_BUTTON_DOWN );
      break;
    case SDL_EVENT_MOUSE_WHEEL:
      onMouseWheel ( event.wheel );
      break;
    default:
      break;
  }
  return true;
}

void SDLAppMain::appQuit ( ) noexcept { onExit ( ); }

SDL_AppResult SDL_AppInit_Impl ( void** appstate, const int argc, char** argv ) noexcept {
  static auto app = sdl::SDLAppMain::init ( argc, argv );
  if ( !app  ) {
    sdl::log::error ( sdl::log::CategoryType::Application,
                      "Failed onInit: Implement the following function "
                      "'std::unique_ptr<sdl::SDLAppMain> sdl::SDLAppMain::init(int argc, char** argv)'" );
    
    return SDL_APP_FAILURE;
  }
  if ( !app->appInit ( argc, argv ) ) {
    sdl::log::warn( sdl::log::CategoryType::Application, "Failed onInit: Application failed to init ");
    return SDL_APP_FAILURE;
  }
  *appstate = app.get ( );
  return SDL_APP_CONTINUE;
}

SDL_AppResult SDL_AppIterate_Impl ( void* appstate ) noexcept {
  if ( auto* app = static_cast< SDLAppMain* > ( appstate ); !app || !app->appIterate ( ) ) {
    return SDL_APP_FAILURE;
  }
  return SDL_APP_CONTINUE;
}

SDL_AppResult SDL_AppEvent_Impl ( void* appstate, SDL_Event* event ) noexcept {
  if ( event->type == SDL_EVENT_QUIT ) {
    return SDL_APP_SUCCESS;
  }

  if ( auto* app = static_cast< SDLAppMain* > ( appstate ); app ) {
    if ( !app->appEvent ( *event ) ) {
      return SDL_APP_SUCCESS;
    }
  }
  return SDL_APP_CONTINUE;
}

void SDL_AppQuit_Impl ( void* appstate, SDL_AppResult result ) noexcept {
  if ( result != SDL_APP_SUCCESS ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed onQuit: FYI something was not successful!!! " );
  }
  if ( auto* app = static_cast< SDLAppMain* > ( appstate ); app ) {
    app->appQuit ( );
  }
}
}  // namespace sdl

extern "C" SDL_AppResult SDLCALL SDL_AppInit ( void** appstate, int argc, char** argv ) {
  //
  return sdl::SDL_AppInit_Impl ( appstate, argc, argv );
}

extern "C" SDL_AppResult SDLCALL SDL_AppIterate ( void* appstate ) {
  //
  return sdl::SDL_AppIterate_Impl ( appstate );
}

extern "C" SDL_AppResult SDLCALL SDL_AppEvent ( void* appstate, SDL_Event* event ) {
  //
  return sdl::SDL_AppEvent_Impl ( appstate, event );
}

extern "C" void SDLCALL SDL_AppQuit ( void* appstate, SDL_AppResult result ) {
  //
  sdl::SDL_AppQuit_Impl ( appstate, result );
}
