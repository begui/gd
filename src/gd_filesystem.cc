#include "gd/gd_filesystem.hh"

#include <physfs.h>

#include <map>
#include <string>

#include "gd/sdl/gd_sdl_core.hh"

namespace gd {

auto error_string = [] ( ) {
#if ( PHYSFS_VER_MAJOR > 2 || ( PHYSFS_VER_MAJOR == 2 && PHYSFS_VER_MINOR >= 1 ) )
  auto errorCode = ::PHYSFS_getLastErrorCode ( );
  return ::PHYSFS_getErrorByCode ( errorCode );
#else
  return ::PHYSFS_getLastError ( );
#endif
};

struct PhysFSDeleter {
  void operator( ) ( PHYSFS_file* ptr ) const {
    if ( ptr != nullptr && 0 == ::PHYSFS_close ( ptr ) ) {
      sdl::log::warn ( sdl::log::CategoryType::Application, "PhysFS: Failed close handle. %s", error_string ( ) );
    }
  }
};

using PhysFSUniqPtr = std::unique_ptr< ::PHYSFS_file, PhysFSDeleter >;

enum class FileModeType {
  READ,
  WRITE,
  APPEND
};
static const std::map< FileModeType, const char* > fileModeTypeKV = {
    { FileModeType::READ, "READ" },
    { FileModeType::WRITE, "WRITE" },
    { FileModeType::APPEND, "APPEND" },
};

inline auto file_r_w_a ( char const* filename, const FileModeType mode ) {
  PHYSFS_File* file = nullptr;
  switch ( mode ) {
    case FileModeType::READ:
      file = PHYSFS_openRead ( filename );
      break;
    case FileModeType::WRITE:
      file = PHYSFS_openWrite ( filename );
      break;
    case FileModeType::APPEND:
      file = PHYSFS_openAppend ( filename );
      break;
    default:
      break;
  }
  if ( file == nullptr ) {
    sdl::log::critical ( sdl::log::CategoryType::Application, "Failed on %s for file %s . %s",
                         fileModeTypeKV.at ( mode ), filename, error_string ( ) );
  }
  return PhysFSUniqPtr{ file };
}

inline auto file_length ( PHYSFS_file* ptr ) {
  auto rtn = ::PHYSFS_fileLength ( ptr );
  if ( rtn == -1 ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Unable to determine file length." );
  }
  return rtn;
}

inline auto stat ( const std::string& fname ) {
  PHYSFS_Stat stat;
  ::PHYSFS_stat ( fname.c_str ( ), &stat );
  return stat;
}

bool FileSystem::init ( const char* basePath ) noexcept {
  if ( 0 == ::PHYSFS_init ( basePath ) ) {
    sdl::log::critical ( sdl::log::CategoryType::Application, "PhysFS: Failed to Initialize.. %s", error_string ( ) );
    return false;
  }
  if ( 0 == PHYSFS_mount ( std::getenv ( "GD_ASSET_PATH" ), nullptr, 1 ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to mount GD_ASSET_PATH" );
  }
  return true;
}

bool FileSystem::isInit ( ) noexcept {
  auto rtn = PHYSFS_isInit ( );
  if ( 0 == rtn ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "PhysFS: is not initialized " );
  }
  return rtn != 0;
}

void FileSystem::destroy ( ) noexcept {
  if ( 0 == PHYSFS_deinit ( ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "PhysFS: Failed to deinitialize.. %s", error_string ( ) );
  }
}

std::tuple< ByteDataUniqPtr, std::size_t > FileSystem::load ( const char* file ) noexcept {
  if ( !fileExists ( file ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "File %s DNE.", file );
    return { nullptr, 0 };
  }
  if ( auto physfsFile = file_r_w_a ( file, FileModeType::READ ); physfsFile != nullptr ) {
    if ( const PHYSFS_sint64 size = file_length ( physfsFile.get ( ) ); size > 0L ) {
      auto buffer = new uint8_t[ size + 1 ];
      if ( size != ::PHYSFS_readBytes ( physfsFile.get ( ), buffer, size ) ) {
        delete[] ( buffer );
        sdl::log::critical ( sdl::log::CategoryType::Application, "PhysFS: File System error while reading file... %s",
                             error_string ( ) );
        return { nullptr, 0LL };
      }
      buffer[ size ] = 0;
      return std::make_tuple ( std::unique_ptr< uint8_t[] > ( buffer ), size );
    }
  }
  return { nullptr, 0LL };
}

bool FileSystem::fileExists ( const char* file ) noexcept {
  const auto rtn = stat ( file );
  return rtn.filetype == PHYSFS_FILETYPE_REGULAR;
}

bool FileSystem::dirExists ( const char* dir ) noexcept {
  const auto rtn = stat ( dir );
  return rtn.filetype == PHYSFS_FILETYPE_DIRECTORY;
}

bool FileSystem::unmount ( [[maybe_unused]] const char* archiveOrDir ) noexcept {
  return PHYSFS_unmount ( archiveOrDir ) != 0;
}

bool FileSystem::mountDirectory ( const char* directory ) noexcept {
  if ( const char* assetPath = std::getenv ( "GD_ASSET_PATH" ); assetPath ) {
    std::string fullPath = std::string ( assetPath ) + std::string ( directory );
    return mount ( fullPath.c_str ( ), nullptr );
  }
  return mount ( directory, nullptr );
}
bool FileSystem::mountArchive ( const char* archive, const char* mountpoint ) noexcept {
  return mount ( archive, mountpoint );
}

bool FileSystem::mount ( [[maybe_unused]] const char* archiveOrDir, [[maybe_unused]] const char* mountpoint ) noexcept {
  auto rtn = 0;
  if ( const char* assetPath = std::getenv ( "GD_ASSET_PATH" ) ) {
    std::string fullPath     = assetPath;
    const char pathSeparator = '/';
    if ( !fullPath.empty ( ) && fullPath.back ( ) != pathSeparator ) {
      fullPath += pathSeparator;  // Append separator if missing
    }
    fullPath += archiveOrDir;
    rtn = PHYSFS_mount ( fullPath.c_str ( ), mountpoint, 1 );
  } else {
    rtn = PHYSFS_mount ( archiveOrDir, mountpoint, 1 );
  }
  if ( 0 == rtn ) {
    sdl::log::critical( sdl::log::CategoryType::Application, "PhysFS: Failed to mount.. %s -- %s", archiveOrDir,
                     error_string ( ) );
  }

  return rtn != 0;
}

}  // namespace gd
