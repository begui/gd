#include "gd/gd_timer.hh"

#include "gd/sdl/gd_sdl_core.hh"

namespace gd {
uint64_t Timer::counter ( ) const noexcept { return sdl::clock::counter ( ); }
uint64_t Timer::frequency ( ) const noexcept { return sdl::clock::frequency ( ); }
uint64_t Timer::tick ( ) const noexcept { return sdl::clock::tick ( ); }
}  // namespace gd