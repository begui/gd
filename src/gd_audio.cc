#include "gd/gd_audio.hh"

#include <entt/entt.hpp>
#include <functional>
#include <unordered_map>
#include <vector>


namespace gd {

constexpr auto NOTPLAYING{ -1 };
class Audio::Impl final {
  friend class Audio;

};

Audio::Audio (  ) noexcept : impl_ ( ) {}
Audio::~Audio ( ) {}  // Required for PIMPL
Audio::Impl& Audio::impl ( ) { return *impl_; }
Audio::Impl& Audio::impl ( ) const { return *impl_; }

std::function< void ( int ) > channel_callback ( ) {
  return [] ( [[maybe_unused]] int channel ) {
    // TODO(bj): do something here?
  };
}

bool Audio::load ( [[maybe_unused]] const Audio::Type audioType,  //
                   [[maybe_unused]] int32_t id,                   //
                   [[maybe_unused]] const char * file ) {
  bool rtn{ false };
  return rtn;
}

void Audio::unload ( const Audio::Type /*audioType*/, const int32_t /*id*/ ) {
  // TODO: Implement
  // We shouldn't delete if it's being played
}

void Audio::play ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] int32_t id,
                   [[maybe_unused]] const int32_t msFadeIn, [[maybe_unused]] const bool repeat ) const {
}  // namespace gd

void Audio::stop ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] int32_t id,
                   [[maybe_unused]] const int32_t msFade ) const {
}

void Audio::pause ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] int32_t id,
                    [[maybe_unused]] const bool pause ) const {
}

void Audio::volume ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] int32_t id,
                     [[maybe_unused]] const int32_t vol ) const {
}

void Audio::volumeUp ( const Audio::Type audioType, int32_t id, const int32_t vol ) const {
  volume ( audioType, id, vol );
}

void Audio::volumeDown ( const Audio::Type audioType, int32_t id, const int32_t vol ) const {
  volume ( audioType, id, vol );
}
void Audio::setSfxChannels ( [[maybe_unused]] int32_t channel ) const {

}
int32_t Audio::getSfxChannels ( ) const {
  auto rtn{ 0 };
  return rtn;
}
void Audio::freeSfxChannes ( ) const { setSfxChannels ( 0 ); }

}  // namespace gd
