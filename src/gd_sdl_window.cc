#include "gd/sdl/gd_sdl_window.hh"
#include <numeric>

namespace sdl {

enum class WindowStateType {
  None,  //
  Fullscreen,
  Minimize,
  Maximize,
  Restore,
  Hide,
  Show,
  Raise
};

inline auto set_window_state ( SDL_Window *window, const WindowStateType state ) noexcept {
  switch ( state ) {
    case WindowStateType::None:
      ::SDL_SetWindowFullscreen ( window, false);
      break;
    case WindowStateType::Fullscreen:
      ::SDL_SetWindowFullscreen ( window, true);
      break;
    case WindowStateType::Show:
      ::SDL_ShowWindow ( window );
      break;
    case WindowStateType::Hide:
      ::SDL_HideWindow ( window );
      break;
    case WindowStateType::Maximize:
      ::SDL_MaximizeWindow ( window );
      break;
    case WindowStateType::Minimize:
      ::SDL_MinimizeWindow ( window );
      break;
    case WindowStateType::Restore:
      ::SDL_RestoreWindow ( window );
      break;
    case WindowStateType::Raise:
      ::SDL_RaiseWindow ( window );
      break;
    default:
      break;
  }
}

Window::Window ( int32_t width, int32_t height, int32_t flags ) noexcept { init ( width, height, flags ); }

bool Window::init ( int32_t width, int32_t height, int32_t flags ) noexcept {
  window_ = sdl::WindowUniqPtr ( SDL_CreateWindow ( nullptr, width, height, flags ) );
  return initialized ( );
}

bool Window::initialized ( ) const noexcept { return window_ != nullptr; }

Window &Window::title ( const std::string &title ) noexcept {
  ::SDL_SetWindowTitle ( window_.get ( ), title.c_str ( ) );
  return *this;
}
Window &Window::center ( ) noexcept {
  position ( SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED );
  return *this;
}
Window &Window::position ( const int x, const int y ) noexcept {
  ::SDL_SetWindowPosition ( window_.get ( ), x, y );
  return *this;
}
Window &Window::opacity ( float op ) noexcept {
  ::SDL_SetWindowOpacity ( window_.get ( ), op );
  return *this;
}
Window &Window::fullscreen ( bool flag ) noexcept {
  set_window_state ( window_.get ( ), flag ? WindowStateType::Fullscreen : WindowStateType::None );
  return *this;
}
Window &Window::show ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Show );
  return *this;
}
Window &Window::hide ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Hide );
  return *this;
}
Window &Window::raise ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Raise );
  return *this;
}
Window &Window::maximize ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Maximize );
  return *this;
}
Window &Window::minimize ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Minimize );
  return *this;
}
Window &Window::restore ( ) noexcept {
  set_window_state ( window_.get ( ), WindowStateType::Restore );
  return *this;
}

std::tuple< int32_t, int32_t > Window::size ( ) const noexcept {
  int w, h;
  SDL_GetWindowSize ( window_.get ( ), &w, &h );
  return { w, h };
}

std::tuple< float, float > Window::sizef ( ) const noexcept {
  auto [ w, h ] = size ( );
  return { static_cast< float > ( w ), static_cast< float > ( h ) };
}

std::tuple< int32_t, int32_t > Window::getAspectRatio ( int32_t width, int32_t height ) {
  if ( const auto gcd = std::gcd ( width, height ); gcd != 0 ) {
    return { width / gcd, height / gcd };
  }
  return { 0, 0 };
}

uint32_t Window::pixelFormat ( ) const noexcept { return SDL_GetWindowPixelFormat ( window_.get ( ) ); }

}  // namespace sdl