
#include "gd/gd_app_sdl.hh"

#include "gd/gd_filesystem.hh"

namespace gd {

bool SDLApp::onInit( [[maybe_unused]] const int argc, [[maybe_unused]] char* argv[] ) noexcept {
  bool initialized = false;
  initialized      = sdl::init ( options.getInitFlags ( ) );
  // Create filesystem
  if ( initialized ) {
    initialized = gd::FileSystem::init ( argv[ 0 ] );
  }
  if ( initialized ) {
    // Create Window
    const auto windowFlags = options.getWindowFlags ( );
    const auto [ ww, wh ]  = options.getWindowSize ( );
    sdl::log::debug ( sdl::log::CategoryType::Application, "Set Window Size to: %d X %d", ww, wh );
    Locator::window::locator::emplace< sdl::Window > ( ww, wh, windowFlags );
    if ( !Window ( ).initialized ( ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to create Window.  %s", SDL_GetError ( ) );
      initialized = false;
    }
  }

  // Create Renderer
  if ( initialized ) {
    Locator::renderer::locator::emplace< sdl::Renderer > ( Window ( ) );
    if ( !Renderer ( ).initialized ( ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to create Renderer.  %s", SDL_GetError ( ) );
      initialized = false;
    }

    if ( initialized ) {
      Renderer ( ).setVSync ( true );  // assume vsync is on, developer can always turn this off
      const auto [ rw, rh ] = options.getRendererSize ( );
      if ( rw > 0 && rh > 0 ) {
        sdl::log::debug ( sdl::log::CategoryType::Application, "Set Logical Size to: %d X %d", rw, rh );
        auto presentation = options.getRendererPresentation ( );
        Renderer ( ).setLogicalSize ( rw, rh, presentation);
        
      }
    }
  }

  // // Init audio
  // gd::Locator::audio::locator::emplace< Audio > ( this );

#if GD_USE_IMGUI
  if ( initialized ) {
    IMGUI_CHECKVERSION ( );
    ImGui::CreateContext ( );
    ImGui::StyleColorsDark ( );
    ImGui_ImplSDL3_InitForSDLRenderer ( Window ( ), Renderer ( ) );
    ImGui_ImplSDLRenderer3_Init ( Renderer ( ) );
  }
#endif

  sdl::log::log_system_info ( );
  return init(argc, argv);
}

void SDLApp::onExit() noexcept {
  destroy ( );

#if GD_USE_IMGUI
  //FIXME: what if we fail to init
  ImGui_ImplSDLRenderer3_Shutdown ( );
  ImGui_ImplSDL3_Shutdown ( );
  ImGui::DestroyContext ( );
#endif

  Locator::renderer::reset ( );
  Locator::window::reset ( );
  FileSystem::destroy();
}

void SDLApp::onUpdate ( float delta ) noexcept {
  update ( delta );
  //
  if ( !currentScene_.expired ( ) ) {
    currentScene_.lock ( )->updateFixed ( delta );
  }
  //  
  if ( sceneTransition_ && sceneTransition_->isTransitioning ( ) ) {
    sceneTransition_->update ( delta );
    if ( sceneTransition_->isComplete ( ) ) {
      sceneTransition_->setTransition ( false );
      sceneTransition_->onExit ( );
      currentScene_ = sceneTransition_->getToScene ( );
    }

  } else if ( !currentScene_.expired ( ) ) {
    currentScene_.lock ( )->update ( delta );
  }
}

void SDLApp::onRender( float delta, float interpolation  ) noexcept {
#if GD_USE_IMGUI
  // Start the Dear ImGui frame
  ImGui_ImplSDLRenderer3_NewFrame ( );
  ImGui_ImplSDL3_NewFrame ( );
  ImGui::NewFrame ( );
#endif
  //
  Renderer ( ).clear ( );
  render ( delta, interpolation);

  if ( sceneTransition_ && sceneTransition_->isTransitioning ( ) ) {
    sceneTransition_->render ( );
  } else if ( auto lock = currentScene_.lock ( ) ) {
    lock->render ( );
  }

#if GD_USE_IMGUI
  ImGui::Render ( );
  ImGui_ImplSDLRenderer3_RenderDrawData ( ImGui::GetDrawData ( ), Renderer ( ) );
#endif

  Renderer ( ).swap ( );
}

void SDLApp::onEvent( SDL_Event &event) noexcept {
  SDL_ConvertEventToRenderCoordinates(Renderer(), &event);
} 


void SDLApp::setTransitionTo ( std::shared_ptr< SceneTransition > transition ) noexcept {
  if ( transition ) {
    sceneTransition_.reset ( );
    sceneTransition_ = transition;
  }
}

void SDLApp::changeSceneTo ( int32_t sceneId, std::shared_ptr< SceneTransition > transition ) noexcept {
  if ( auto scene = scenes_[ sceneId ]; scene != nullptr ) {
    setTransitionTo ( transition );
    if ( sceneTransition_ ) {
      sceneTransition_->start ( currentScene_, scenes_[ sceneId ] );
      sceneTransition_->onEnter ( );
    } else {
      if ( !currentScene_.expired ( ) ) {
        currentScene_.lock ( )->onExit ( );
      }
      currentScene_ = scenes_[ sceneId ];
      currentScene_.lock ( )->onEnter ( );
    }
  }
}

}  // namespace gd
