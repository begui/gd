#include <entt/entt.hpp>

#include <SDL3/SDL_render.h>
#ifdef GD_USE_SDL_IMAGE
#include <SDL3_image/SDL_image.h>
#endif
#include "gd/gd_filesystem.hh"
#include "gd/sdl/gd_sdl_renderer.hh"
#include "gd/sdl/gd_sdl_window.hh"
#include "gd/sdl/gd_sdl_types.hh"

namespace sdl {

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//
// Texture Loaders
//
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
struct TextureResource final {
  explicit TextureResource ( sdl::TextureUniqPtr &&ptr ) : TextureResource ( std::move ( ptr ), nullptr, 0 ) {}

  TextureResource ( sdl::TextureUniqPtr &&ptr, gd::ByteDataUniqPtr &&textureData, size_t textureSize )
      : texturePtr ( std::move ( ptr ) ), byteData ( std::move ( textureData ) ), byteSize ( textureSize ) {}

  explicit operator SDL_Texture * ( ) { return texturePtr.get ( ); }
  explicit operator SDL_Texture * ( ) const { return texturePtr.get ( ); }

  sdl::TextureUniqPtr texturePtr{ nullptr };
  gd::ByteDataUniqPtr byteData{ nullptr };
  std::size_t byteSize{ 0 };
};
struct TextureResourceLoader final {
  using result_type = std::shared_ptr< TextureResource >;

  // static
  result_type operator( ) ( SDL_Renderer *renderer, const char *filename ) const {
    if ( auto [ resource, size ] = gd::FileSystem::load ( filename ); resource != nullptr && size > 0LL ) {
      auto rw = ::SDL_IOFromConstMem ( resource.get ( ), size );
      if ( !rw ) {
        sdl::log::error ( sdl::log::CategoryType::Application, "Failed to read texture from memory. %s ", filename );
        return nullptr;
      }
#if GD_USE_SDL_IMAGE
      auto texturePtr = sdl::TextureUniqPtr ( IMG_LoadTexture_IO ( renderer, rw, true ) );
#else
      auto surfacePtr = sdl::SurfaceUniqPtr ( ::SDL_LoadBMP_IO ( rw, true ) );
      if ( !surfacePtr ) {
        sdl::log::error ( sdl::log::CategoryType::Application, "Failed to create Texture Surface: %s. %s", filename,
                          SDL_GetError ( ) );
        return nullptr;
      }
      auto texturePtr = sdl::TextureUniqPtr ( SDL_CreateTextureFromSurface ( renderer, surfacePtr.get ( ) ) );
#endif
      if ( !texturePtr ) {
        sdl::log::error ( sdl::log::CategoryType::Application, "Failed to create Texture: %s. %s", filename,
                          SDL_GetError ( ) );
        return nullptr;
      }

      return std::make_shared< TextureResource > ( std::move ( texturePtr ), std::move ( resource ), size );
    }
    return nullptr;
  }

  // target
  result_type operator( ) ( SDL_Renderer *renderer, const int32_t w, const int32_t h, const SDL_PixelFormat pixelFormat,
                            const std::function< void ( ) > &func ) const {
    sdl::log::info ( sdl::log::CategoryType::Application, "Loading TextureTarget " );

    // THis can be null if this is the current Rendering target. Being null is ok.
    auto currentTexturePtr = ::SDL_GetRenderTarget ( renderer );

    auto newTexturePtr =
        sdl::TextureUniqPtr ( SDL_CreateTexture ( renderer, pixelFormat, SDL_TEXTUREACCESS_TARGET, w, h ) );
    if ( !newTexturePtr ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to load texture target %s", SDL_GetError ( ) );
      return nullptr;
    }

    if ( !::SDL_SetRenderTarget ( renderer, newTexturePtr.get ( ) ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to SetRenderTarget 1 %s", SDL_GetError ( ) );
      return nullptr;
    }

    if ( !::SDL_SetRenderDrawColor ( renderer, 0, 0, 0, 255 ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to SetRenderDrawColor %s", SDL_GetError ( ) );
      return nullptr;
    }
    if ( !::SDL_RenderClear ( renderer ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to RenderClear %s", SDL_GetError ( ) );
      return nullptr;
    }

    func ( );

    if ( !::SDL_SetRenderTarget ( renderer, currentTexturePtr ) ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to SetRenderTarget 2 %s", SDL_GetError ( ) );
      return nullptr;
    }

    return std::make_shared< TextureResource > ( std::move ( newTexturePtr ) );
  }
  // streaming
  result_type operator( ) ( SDL_Renderer *renderer, int32_t w, int32_t h, SDL_PixelFormat pixelFormat ) const {
    auto ptr = sdl::TextureUniqPtr ( SDL_CreateTexture ( renderer, pixelFormat, SDL_TEXTUREACCESS_STREAMING, w, h ) );
    return std::make_shared< TextureResource > ( std::move ( ptr ) );
  }
};

using TextureCache = entt::resource_cache< TextureResource, TextureResourceLoader >;

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//
// Renderer
//
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
Renderer::Renderer ( const Window &window ) { init ( window ); }

struct Renderer::Impl final {
  sdl::RendererUniqPtr renderer_{ nullptr };
  TextureCache cache_;
  SDL_PixelFormat pixelFormat_{ SDL_PIXELFORMAT_RGBA32 };
};
Renderer::~Renderer ( ) = default;  // Required for PIMPL
Renderer::Impl &Renderer::impl ( ) { return *impl_; }
Renderer::Impl &Renderer::impl ( ) const { return *impl_; }

explicit ( false ) Renderer::operator SDL_Renderer * ( ) { return impl ( ).renderer_.get ( ); }
explicit ( false ) Renderer::operator SDL_Renderer * ( ) const { return impl ( ).renderer_.get ( ); }

bool Renderer::init ( const Window &window, const char *name ) noexcept {
  impl ( ).renderer_ = sdl::RendererUniqPtr ( SDL_CreateRenderer ( window, name ) );
  return initialized ( );
}

bool Renderer::initialized ( ) const noexcept { return impl ( ).renderer_ != nullptr; }

std::tuple< std::int32_t, std::int32_t > Renderer::size ( ) const noexcept {
  int w{ 0 };
  int h{ 0 };
  if ( !SDL_GetCurrentRenderOutputSize( impl ( ).renderer_.get ( ), &w, &h ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to get renderer size. %s", SDL_GetError ( ) );
  }
  return { w, h };
}

std::tuple< float, float > Renderer::sizef ( ) const noexcept {
  auto [ w, h ] = size ( );
  return { static_cast< float > ( w ), static_cast< float > ( h ) };
}


sdl::Recti32_t Renderer::getViewPort ( ) const noexcept {
  sdl::Recti32_t rect;
  ::SDL_GetRenderViewport ( impl ( ).renderer_.get ( ), &rect );
  return rect;
}

void Renderer::setViewPort ( const sdl::Recti32Opt_t &rectOpt ) noexcept {
  const SDL_Rect *rect = rectOpt.has_value ( ) ? &rectOpt.value ( ) : nullptr;

  if ( !::SDL_SetRenderViewport ( impl ( ).renderer_.get ( ), rect ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set Viewport. %s", SDL_GetError ( ) );
  }
}

sdl::Rectf_t Renderer::getLogicalRect() const noexcept{
  sdl::Rectf_t prezRect;
  if(!::SDL_GetRenderLogicalPresentationRect(impl().renderer_.get(), &prezRect)) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to get renderer logical rect size. %s", SDL_GetError ( ) );
  }
  return prezRect;
}

std::tuple<int32_t, int32_t> Renderer::getLogicalSize() const noexcept {
  int32_t w{0}, h{0};
  if ( !::SDL_GetRenderLogicalPresentation( impl ( ).renderer_.get ( ), &w, &h, nullptr ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to get renderer logical size. %s", SDL_GetError ( ) );
  }
  return {w, h};
}

void Renderer::setLogicalSize ( const std::int32_t width, const std::int32_t height,
                                PresentationType presentationMode ) const noexcept {
  const auto mode = static_cast< SDL_RendererLogicalPresentation > ( presentationMode );
  if ( !::SDL_SetRenderLogicalPresentation ( impl ( ).renderer_.get ( ), width, height, mode ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set renderer logical size. %s", SDL_GetError ( ) );
  }
}

std::tuple<float, float> Renderer::getScale() const noexcept {
  float scaleX{0.f}, scaleY{0.f};
  if(!::SDL_GetRenderScale(impl().renderer_.get(), &scaleX, &scaleY)) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to get renderer scale. %s", SDL_GetError ( ) );
  }
  return {scaleX, scaleY};
}

void Renderer::setScale ( float scaleX, float scaleY ) const noexcept {
  if ( !::SDL_SetRenderScale ( impl ( ).renderer_.get ( ), scaleX, scaleY ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set renderer scale. %s", SDL_GetError ( ) );
  }
}

std::tuple< float, float > Renderer::logicalMousePosition ( ) const noexcept {
  auto [ wX, wY ] = sdl::MOUSE::position ( );
  float logicalX{ 0.f };
  float logicalY{ 0.f };
  ::SDL_RenderCoordinatesFromWindow ( impl ( ).renderer_.get ( ), wX, wY, &logicalX, &logicalY );
  return { logicalX, logicalY };
}

void Renderer::setVSync ( bool enable ) noexcept {
  if ( !::SDL_SetRenderVSync ( impl ( ).renderer_.get ( ), enable ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set VSync. %s", SDL_GetError ( ) );
  }
}

void Renderer::clear ( ) const noexcept {
  // if ( 0 != ::SDL_SetRenderDrawColorFloat ( impl ( ).renderer_.get ( ), 0.f, 0.f, 0.f, 255.f ) ) {
  //   sdl::log::error ( sdl::log::CategoryType::Application, "Failed to SetRenderDrawColor %s", SDL_GetError ( ) );
  // }
  //
  if ( !::SDL_RenderClear ( impl ( ).renderer_.get ( ) ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to RenderClear %s", SDL_GetError ( ) );
  }
}

void Renderer::swap ( ) const noexcept { ::SDL_RenderPresent ( impl ( ).renderer_.get ( ) ); }



BlendmodeType Renderer::getBlendMode ( ) const noexcept {
  SDL_BlendMode blendmode;
  if ( !::SDL_GetRenderDrawBlendMode ( impl ( ).renderer_.get ( ), &blendmode ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to get blendmode from renderer. %s",
                      SDL_GetError ( ) );
    return static_cast< BlendmodeType > (
        SDL_BLENDMODE_NONE );  // Ensure to return a default or error value of BlendmodeType
  }
  return static_cast< BlendmodeType > ( blendmode );  // Ensure correct casting to BlendmodeType
}

void Renderer::setBlendMode ( BlendmodeType blendmode ) const noexcept {
  if ( !::SDL_SetRenderDrawBlendMode ( impl ( ).renderer_.get ( ), static_cast< SDL_BlendMode > ( blendmode ) ) ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set blend mode. %s", SDL_GetError ( ) );
  }
}

template < ColorType COLORT >
void Renderer::setColor ( const COLORT &color ) const noexcept {
  bool rtn = { false };
  if constexpr ( std::same_as< COLORT, sdl::Color > ) {
    rtn = ::SDL_SetRenderDrawColor ( impl ( ).renderer_.get ( ), color.r, color.g, color.b, color.a );
  } else if constexpr ( std::same_as< COLORT, sdl::Colorf > ) {
    rtn = ::SDL_SetRenderDrawColorFloat ( impl ( ).renderer_.get ( ), color.r, color.g, color.b, color.a );
  }
  //
  if ( !rtn ) {
    sdl::log::error ( sdl::log::CategoryType::Application, "Failed to set set color. %s", SDL_GetError ( ) );
  }
}

template < typename FUNC, ColorType COLORT >
inline void draw_call ( SDL_Renderer *renderer, FUNC func, const COLORT &color ) noexcept {
  bool rtn = { false };
  SDL_BlendMode prevMode;
  COLORT prevColor;
  if constexpr ( std::same_as< COLORT, sdl::Color > ) {
    ::SDL_GetRenderDrawColor ( renderer, &prevColor.r, &prevColor.g, &prevColor.b, &prevColor.a );
    ::SDL_GetRenderDrawBlendMode ( renderer, &prevMode );
    ::SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
    rtn = func ( );
    ::SDL_SetRenderDrawColor ( renderer, prevColor.r, prevColor.g, prevColor.b, prevColor.a );
    ::SDL_SetRenderDrawBlendMode ( renderer, prevMode );
  } else if constexpr ( std::same_as< COLORT, sdl::Colorf > ) {
    ::SDL_GetRenderDrawColorFloat ( renderer, &prevColor.r, &prevColor.g, &prevColor.b, &prevColor.a );
    ::SDL_GetRenderDrawBlendMode ( renderer, &prevMode );
    ::SDL_SetRenderDrawColorFloat ( renderer, color.r, color.g, color.b, color.a );
    rtn = func ( );
    ::SDL_SetRenderDrawColorFloat ( renderer, prevColor.r, prevColor.g, prevColor.b, prevColor.a );
    ::SDL_SetRenderDrawBlendMode ( renderer, prevMode );
  }
  //
  if ( !rtn ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to Render" );
  }
}

template < ColorType COLORT >
void Renderer::drawBackground ( const COLORT &color ) const noexcept {  //
  SDL_Renderer *renderer = impl ( ).renderer_.get ( );
  draw_call ( renderer, [ & ] ( ) { return ::SDL_RenderFillRect ( renderer, nullptr ); }, color );
}

template < ColorType COLORT >
void Renderer::drawPoint ( const sdl::Pointf_t &point, const COLORT &color ) const noexcept {
  SDL_Renderer *renderer = impl ( ).renderer_.get ( );
  draw_call ( renderer, [ & ] ( ) { return ::SDL_RenderPoint ( renderer, point.x, point.y ); }, color );
}

template < ColorType COLORT >
void Renderer::drawPoints ( const std::vector< sdl::Pointf_t > &points, const COLORT &color ) const noexcept {
  if ( !points.empty ( ) ) {
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    draw_call (
        renderer,
        [ & ] ( ) { return ::SDL_RenderPoints ( renderer, &points[ 0 ], static_cast< int > ( points.size ( ) ) ); },
        color );
  }
}

template < ColorType COLORT >
void Renderer::drawLine ( const sdl::Pointf_t &p1, const sdl::Pointf_t &p2, const COLORT &color ) const noexcept {
  SDL_Renderer *renderer = impl ( ).renderer_.get ( );
  draw_call ( renderer, [ & ] ( ) { return ::SDL_RenderLine ( renderer, p1.x, p1.y, p2.x, p2.y ); }, color );
}

template < ColorType COLORT >
void Renderer::drawLineH ( const float y, const float x1, const float x2, const COLORT &color ) const noexcept {
  drawLine ( { x1, y }, { x2, y }, color );
}

template < ColorType COLORT >
void Renderer::drawLineV ( const float x, const float y1, const float y2, const COLORT &color ) const noexcept {
  drawLine ( { x, y1 }, { x, y2 }, color );
}

template < ColorType COLORT >
void Renderer::drawLines ( const std::vector< sdl::Pointf_t > &points, const COLORT &color ) const noexcept {
  if ( !points.empty ( ) ) {
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    draw_call (
        renderer,
        [ & ] ( ) { return ::SDL_RenderLines ( renderer, &points[ 0 ], static_cast< int > ( points.size ( ) ) ); },
        color );
  }
}

template < ColorType COLORT >
void Renderer::drawRect ( const sdl::Rectf_t &rect, const COLORT &color ) const noexcept {  //
  SDL_Renderer *renderer = impl ( ).renderer_.get ( );
  draw_call ( renderer, [ & ] ( ) { return ::SDL_RenderRect ( renderer, &rect ); }, color );
}

template < ColorType COLORT >
void Renderer::drawRects ( const std::vector< sdl::Rectf_t > &rects, const COLORT &color ) const noexcept {
  if ( !rects.empty ( ) ) {
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    draw_call (
        renderer,
        [ & ] ( ) { return ::SDL_RenderRects ( renderer, &rects[ 0 ], static_cast< int > ( rects.size ( ) ) ); },
        color );
  }
}

template < ColorType COLORT >
void Renderer::drawRectFill ( const sdl::Rectf_t &rect, const COLORT &color ) const noexcept {
  SDL_Renderer *renderer = impl ( ).renderer_.get ( );
  draw_call ( renderer, [ & ] ( ) { return ::SDL_RenderFillRect ( renderer, &rect ); }, color );
}

template < ColorType COLORT >
void Renderer::drawRectsFill ( const std::vector< sdl::Rectf_t > &rects, const COLORT &color ) const noexcept {
  if ( !rects.empty ( ) ) {
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    draw_call (
        renderer,
        [ & ] ( ) { return ::SDL_RenderFillRects ( renderer, &rects[ 0 ], static_cast< int > ( rects.size ( ) ) ); },
        color );
  }
}

void Renderer::drawGeometry ( const std::vector< sdl::Vertex > &v ) const noexcept {
  if ( !v.empty ( ) ) {
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    ::SDL_RenderGeometry ( renderer, nullptr, &v[ 0 ], static_cast< int > ( v.size ( ) ), nullptr, 0 );
  }
}

void Renderer::drawTexture ( const gd::cid_t id,          //
                             const sdl::RectfOpt_t &src,  //
                             const sdl::RectfOpt_t &dst,  //
                             const float scale,
                             const sdl::RotationOpt_t &rotation ) const noexcept {
  if ( auto res = impl ( ).cache_[ id ]; res ) {
    const auto srcPtr      = src.has_value ( ) ? &src.value ( ) : nullptr;
    const auto dstPtr      = dst.has_value ( ) ? &dst.value ( ) : nullptr;
    SDL_Renderer *renderer = impl ( ).renderer_.get ( );
    SDL_Texture *texture   = res->texturePtr.get ( );

    bool result{ false };
    if ( rotation.has_value ( ) ) {
      const auto &rotationData = rotation.value ( );
      const auto flip          = gd::typetrait_util::to_type< SDL_FlipMode > ( rotationData.flip );
      const auto angle         = rotationData.angle;
      const auto angleCenter   = rotationData.angleCenter.has_value ( ) ? &rotationData.angleCenter.value ( ) : nullptr;

      result = ::SDL_RenderTextureRotated ( renderer, texture, srcPtr, dstPtr, angle, angleCenter, flip );
    } 
    else if(scale != 0) { 
      result = ::SDL_RenderTextureTiled ( renderer, texture, srcPtr, scale, dstPtr );
    }
    else {
      result = ::SDL_RenderTexture ( renderer, texture, srcPtr, dstPtr );
    }

    if ( !result ) {
      sdl::log::error ( sdl::log::CategoryType::Application, "Failed to render texture. %s", SDL_GetError ( ) );
    }
  }
}

// Generic load function
template < typename... Args >
bool load_texture ( Renderer &renderer, gd::cid_t id, Args &&...args ) noexcept {
  auto rendererPtr       = renderer.impl ( ).renderer_.get ( );
  auto [ iter, success ] = renderer.impl ( ).cache_.force_load ( id, rendererPtr, std::forward< Args > ( args )... );
  if ( !iter->second ) {
    // NOTE:: entt cache will insert a null value in your map... Look for that null and delete it.. something bad
    // happened
    renderer.impl ( ).cache_.erase ( iter->first );
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to load texture. %s", SDL_GetError ( ) );
    return false;
  }
  return ( iter != renderer.impl ( ).cache_.end ( ) );
}

// Static Textures
bool Renderer::loadStaticTexture ( gd::cid_t id, const std::string &filename ) noexcept {
  if ( gd::FileSystem::fileExists ( filename.c_str ( ) ) ) {
    return load_texture ( *this, id, filename.c_str ( ) );
  }
  return false;
}
// Target Textures
bool Renderer::loadTargetTexture ( gd::cid_t id, const std::int32_t w, const std::int32_t h,
                                   const std::function< void ( ) >& func ) noexcept {
  auto pixelformat = impl ( ).pixelFormat_;
  return load_texture ( *this, id, w, h, pixelformat, func );
}
// Streaming Textures
bool Renderer::loadStreamTexture ( gd::cid_t id, const std::int32_t w, const std::int32_t h ) noexcept {
  auto pixelformat = impl ( ).pixelFormat_;
  return load_texture ( *this, id, w, h, pixelformat );
}

bool Renderer::updateStreamTexture ( gd::cid_t id,                                                //
                                     const std::function< void ( std::uint32_t *pixels ) >& func,  //
                                     const sdl::Recti32_t &rect ) noexcept {
  bool rtn{ false };
  if ( auto res = impl ( ).cache_[ id ]; res ) {
    if ( ::SDL_Texture *texture = res->texturePtr.get ( ); texture ) {
      std::uint32_t *pixels{ nullptr };
      std::int32_t pitch{ 0 };
      if ( ( rtn = ( ::SDL_LockTexture ( texture, &rect, ( void ** ) &pixels, &pitch ) == 0 ) ) ) {
        func ( pixels );
        ::SDL_UnlockTexture ( texture );
      }
    }
  }
  return !rtn;
}

void Renderer::unloadTexture ( gd::cid_t id ) noexcept { impl ( ).cache_.erase ( id ); }

std::tuple< float, float > Renderer::textureSize ( gd::cid_t id ) const noexcept {
  float w = 0.f;
  float h = 0.f;
  if ( auto res = impl ( ).cache_[ id ]; res && !::SDL_GetTextureSize ( res->texturePtr.get ( ), &w, &h ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to query Texture." );
  }
  return { w, h };
}

size_t Renderer::textureCacheSize ( ) const noexcept { return impl ( ).cache_.size ( ); }
void Renderer::textureCacheClear ( ) noexcept { impl ( ).cache_.clear ( ); }

void Renderer::texturePixelFormat ( const SDL_PixelFormat format ) { impl ( ).pixelFormat_ = format; }
const char *Renderer::texturePixelFormatName ( ) const { return SDL_GetPixelFormatName ( impl ( ).pixelFormat_ ); }

BlendmodeType Renderer::textureBlendMode ( gd::cid_t id ) const noexcept {
  SDL_BlendMode blendmode;

  if ( auto res = impl ( ).cache_[ id ]; res && !::SDL_GetTextureBlendMode ( res->texturePtr.get ( ), &blendmode ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to get blendmode from renderer. %s",
                     SDL_GetError ( ) );
    // Ensure to return a default or error value of BlendmodeType
    return static_cast< BlendmodeType > ( SDL_BLENDMODE_NONE );
  }
  // BUG: we could get a value that does not exist.
  return static_cast< BlendmodeType > ( blendmode );
}

void Renderer::textureBlendMode ( gd::cid_t id, BlendmodeType blendmode ) noexcept {
  if ( auto res = impl ( ).cache_[ id ];
       res && !::SDL_SetTextureBlendMode ( res->texturePtr.get ( ), static_cast< SDL_BlendMode > ( blendmode ) ) ) {
    sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to set texture blend mode. %s", SDL_GetError ( ) );
  }
}

// template < typename T >
// concept FloatOrUInt8 = std::is_same_v< T, float > || std::is_same_v< T, uint8_t >;

template < gd::FloatOrUInt8 T >
void set_alpha ( Renderer &renderer, gd::cid_t id, const T alpha ) noexcept {
  if ( auto res = renderer.impl ( ).cache_[ id ]; res ) {
    auto texturePtr = res->texturePtr.get ( );
    if constexpr ( std::is_same_v< T, uint8_t > ) {
      if ( !::SDL_SetTextureAlphaMod ( texturePtr, alpha ) ) {
        sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to set texture alpha mode. %s",
                         SDL_GetError ( ) );
      }
    } else if constexpr ( std::is_same_v< T, float > ) {
      if ( !::SDL_SetTextureAlphaModFloat ( texturePtr, alpha ) ) {
        sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to set texture alphaf mode. %s",
                         SDL_GetError ( ) );
      }
    }
  }
}

template < gd::FloatOrUInt8 T >
T get_alpha ( const Renderer &renderer, gd::cid_t id ) noexcept {
  T alpha{ 0 };
  if ( const auto res = renderer.impl ( ).cache_[ id ]; res ) {
    const auto texturePtr = res->texturePtr.get ( );
    if constexpr ( std::is_same_v< T, uint8_t > ) {
      if ( !::SDL_GetTextureAlphaMod ( texturePtr, &alpha ) ) {
        sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to get texture alpha mode. %s",
                         SDL_GetError ( ) );
      }
    } else if constexpr ( std::is_same_v< T, float > ) {
      if ( !::SDL_GetTextureAlphaModFloat ( texturePtr, &alpha ) ) {
        sdl::log::warn ( sdl::log::CategoryType::Application, "Failed to get texture alphaf mode. %s",
                         SDL_GetError ( ) );
      }
    }
  }
  return alpha;
}

void Renderer::textureAlpha ( gd::cid_t id, const uint8_t alpha ) noexcept {
  //
  set_alpha ( *this, id, alpha );
}

uint8_t Renderer::textureAlpha ( gd::cid_t id ) const noexcept {
  //
  return get_alpha< uint8_t > ( *this, id );
}

void Renderer::textureAlphaf ( gd::cid_t id, const float alpha ) noexcept {
  //
  set_alpha ( *this, id, alpha );
}

float Renderer::textureAlphaf ( gd::cid_t id ) const noexcept {
  //
  return get_alpha< float > ( *this, id );
}

// Explicit instantiation of the template for the types you're using
template void Renderer::setColor ( const sdl::Color &color ) const noexcept;
template void Renderer::setColor ( const sdl::Colorf &color ) const noexcept;
template void Renderer::drawBackground ( const sdl::Color &color ) const noexcept;
template void Renderer::drawBackground ( const sdl::Colorf &color ) const noexcept;
template void Renderer::drawPoint ( const sdl::Pointf_t &p1, const Color&color ) const noexcept;
template void Renderer::drawPoint ( const sdl::Pointf_t &p1, const Colorf&color ) const noexcept;
template void Renderer::drawPoints ( const std::vector< sdl::Pointf_t > &pts, const Color&color ) const noexcept;
template void Renderer::drawPoints ( const std::vector< sdl::Pointf_t > &pts, const Colorf&color ) const noexcept;
template void Renderer::drawLine ( const sdl::Pointf_t &p1, const sdl::Pointf_t &p2, const Color&color ) const noexcept;
template void Renderer::drawLine ( const sdl::Pointf_t &p1, const sdl::Pointf_t &p2, const Colorf&color ) const noexcept;
template void Renderer::drawLineH ( const float y, const float x1, const float x2, const Color&color ) const noexcept;
template void Renderer::drawLineH ( const float y, const float x1, const float x2, const Colorf&color ) const noexcept;
template void Renderer::drawLineV ( const float x, const float y1, const float y2, const Color&color ) const noexcept;
template void Renderer::drawLineV ( const float x, const float y1, const float y2, const Colorf&color ) const noexcept;
template void Renderer::drawLines ( const std::vector< sdl::Pointf_t > &pts, const Color&color ) const noexcept;
template void Renderer::drawLines ( const std::vector< sdl::Pointf_t > &pts, const Colorf&color ) const noexcept;
template void Renderer::drawRect ( const sdl::Rectf_t &rect, const Color &color ) const noexcept;
template void Renderer::drawRect ( const sdl::Rectf_t &rect, const Colorf &color ) const noexcept;
template void Renderer::drawRects ( const std::vector< sdl::Rectf_t > &rects, const Color&color ) const noexcept;
template void Renderer::drawRects ( const std::vector< sdl::Rectf_t > &rects, const Colorf&color ) const noexcept;
template void Renderer::drawRectFill ( const sdl::Rectf_t &rect, const Color &color ) const noexcept;
template void Renderer::drawRectFill ( const sdl::Rectf_t &rect, const Colorf &color ) const noexcept;
template void Renderer::drawRectsFill ( const std::vector< sdl::Rectf_t > &rects, const Color&color ) const noexcept;
template void Renderer::drawRectsFill ( const std::vector< sdl::Rectf_t > &rects, const Colorf&color ) const noexcept;

}  // namespace sdl
