
#include "gd/sdl/gd_sdl_core.hh"
#include "gd/sdl/gd_sdl_types.hh"

#include "gd/sdl/gd_sdl_image.hh"
#include "gd/sdl/gd_sdl_ttf.hh"
#include "gd/sdl/gd_sdl_mixer.hh"

#include <SDL3/SDL_version.h>

#include <cmath>
#include <tuple>

using namespace gd;
namespace sdl {

[[nodiscard]]
bool init ( uint32_t flags ) noexcept {
#ifdef __EMSCRIPTEN__
  ////Note: There must be another flag that is causing it to throw an error
  // flags &= ~( internal::enum_class_or ( InitType::Haptic ) );
  //// Forcing the list of flags that work with EmScripten
  flags = internal::enum_class_or ( InitType::Video, InitType::Timer, InitType::Events, InitType::Controller,
                                    InitType::Joystick, InitType::Audio );
#endif
  if ( !::SDL_Init ( flags ) ) {
    sdl::log::critical ( sdl::log::CategoryType::Application, "Failed to initialize %s: %s", sdl::SubSystem,
                         SDL_GetError ( ) );
    return false;
  }
  std::atexit ( [] ( ) {
    sdl::log::debug ( sdl::log::CategoryType::Application, "Quitting SDL" );
    ::SDL_Quit ( );
  } );
  return true;
}

[[nodiscard]]
auto get_version ( SubsystemType subSystem ) noexcept {
  switch ( subSystem ) {
    case SubsystemType::Core:
      return SDL_GetVersion ( );
      break;
    case SubsystemType::Image:
#if GD_USE_SDL_IMAGE
      return IMG_Version ( );
#endif
      break;
    case SubsystemType::Ttf:
#if GD_USE_SDL_TTF
      return TTF_Version ( );
#endif
      break;
    case SubsystemType::Audio:
#if GD_USE_SDL_MIXER
      return Mix_Version ( );
#endif
      break;
  }
  return 0;
}

///
// Log
///
namespace log {
void log_displays ( ) noexcept {
  int32_t nVideoDisplays = 0;
  SDL_GetDisplays ( &nVideoDisplays );

  // TODO: This could be wrong

  for ( auto videoDisplayIndex = 0; videoDisplayIndex < nVideoDisplays; ++videoDisplayIndex ) {
    if ( const SDL_DisplayMode *mode = SDL_GetCurrentDisplayMode ( videoDisplayIndex ); mode != nullptr ) {
      sdl::log::info ( sdl::log::CategoryType::Application, "Current Display #%d: display mode is %dx%dpx @ %dhz.",
                       videoDisplayIndex, mode->w, mode->h, mode->refresh_rate );
    }
    if ( const SDL_DisplayMode *mode = SDL_GetDesktopDisplayMode ( videoDisplayIndex ); mode != nullptr ) {
      sdl::log::info ( sdl::log::CategoryType::Application, "Desktop Display #%d: display mode is %dx%dpx @ %dhz.",
                       videoDisplayIndex, mode->w, mode->h, mode->refresh_rate );
    }
    // TODO: Fix this

    //   const auto nDisplayModes = SDL_GetFullscreenDisplayModes ( videoDisplayIndex );
    //   for ( auto displayModeIndex{ 0 }; displayModeIndex < nDisplayModes; ++displayModeIndex ) {
    //     if ( 0 == SDL_GetDisplayMode ( videoDisplayIndex, displayModeIndex, &mode ) ) {
    //       const auto [ aspectw, aspecth ] = Window::getAspectRatio ( mode.w, mode.h );
    //       sdl::log::info ( sdl::log::CategoryType::Application,
    //                         "Supported Display #%d: display mode is %dx%dpx @ %dhz. [%d:%d]", videoDisplayIndex,
    //                         mode.w, mode.h, mode.refresh_rate, aspectw, aspecth );
    //     }
    //   }
  }
}
void log_system_info ( ) noexcept {
  // Note: eventually might want to make this easier to parse
  log::log ( "Platform %s\n", system::platform ( ) );
  log::log ( "Cpu Count %d\n", system::cpu::count ( ) );
  log::log ( "Ram %d\n", system::ram ( ) );
  log::log ( "Current Video Driver %s\n", system::video_driver ( ) );
  const auto numVideoDrivers = SDL_GetNumVideoDrivers ( );
  log::log ( "All Video Drivers " );
  for ( auto i = 0; i < numVideoDrivers; ++i ) {
    log::log ( "    %s\n", system::video_driver ( i ) );
  }

  {
    auto version = sdl::get_version ( sdl::SubsystemType::Core );
    log::log ( "SDL CORE %d", version );
  }

#if GD_USE_SDL_TTF
  {
    auto version = sdl::get_version ( sdl::SubsystemType::Ttf );
    log::log ( "SDL TTF %d", version );
  }
#endif

#if GD_USE_SDL_IMAGE
  {
    auto version = sdl::get_version ( sdl::SubsystemType::Image );
    log::log ( "SDL IMAGE %d", version );
  }
#endif

#if GD_USE_SDL_MIXER
  {
    auto version = sdl::get_version ( sdl::SubsystemType::Audio );
    log::log ( "SDL MIXER %d", version );
  }
#endif
}

}  // namespace log

///
// Point
///
template struct Point< int32_t >;
template struct Point< float >;

template < typename T >
bool Point< T >::operator== ( const Point &other ) const {  //
  return ( this->x == other.x ) && ( this->y == other.y );
}

template < typename T >
bool Point< T >::operator< ( const Point &other ) const {  //
  return ( ( this->x < other.x ) || ( ( this->x == other.x ) && ( this->y < other.y ) ) );
}
template < typename T >
void Point< T >::operator*= ( const Point &other ) {
  this->x *= other.x;
  this->y *= other.y;
}
template < typename T >
void Point< T >::operator/= ( const Point &other ) {
  this->x /= other.x;
  this->y /= other.y;
}
///
// Rect
///
template struct Rect< int32_t >;
template struct Rect< float >;

template < typename T>
bool Rect< T >::isEmpty ( ) const noexcept {
  if constexpr ( std::is_floating_point_v< T > ) {
    return ::SDL_RectEmptyFloat ( this );
  } else {
    return ::SDL_RectEmpty ( this );
  }
}

template < typename T>
bool Rect< T >::pointInRect ( const Point< T > &other ) const noexcept {
  if constexpr ( std::is_floating_point_v< T > ) {
    return ::SDL_PointInRectFloat ( &other, this );
  } else {
    return ::SDL_PointInRect ( &other, this );
  }
}

template < typename T>
bool Rect< T >::hasIntersection ( const Rect< T > &other ) const noexcept {
  if constexpr ( std::is_floating_point_v< T > ) {
    return ::SDL_HasRectIntersectionFloat ( &other, this );
  } else {
    return ::SDL_HasRectIntersection ( &other, this );
  }
}
///
// Color
///
Color::Color ( uint8_t _r, uint8_t _g, uint8_t _b, uint8_t _a )
    : SDL_Color{ static_cast< Uint8 > ( _r % 256u ), static_cast< Uint8 > ( _g % 256u ),
                 static_cast< Uint8 > ( _b % 256u ), static_cast< Uint8 > ( _a % 256u ) } {}
void Color::red ( uint8_t _r ) { r = _r % 256u; }
void Color::green ( uint8_t _g ) { g = _g % 256u; }
void Color::blue ( uint8_t _b ) { b = _b % 256u; }
void Color::alpha ( uint8_t _a ) { a = _a % 256u; }
void Color::rgba ( uint8_t _r, uint8_t _g, uint8_t _b, uint8_t _a ) {
  red ( _r );
  green ( _g );
  blue ( _b );
  alpha ( _a );
}
uint8_t Color::red ( ) const { return r; }
uint8_t Color::green ( ) const { return g; }
uint8_t Color::blue ( ) const { return b; }
uint8_t Color::alpha ( ) const { return a; }

Color::operator uint32_t ( ) const { return ( red ( ) << 24 ) | ( green ( ) << 16 ) | ( blue ( ) << 8 ) | alpha ( ); }
bool Color::operator== ( const Color &other ) const {
  return other.r == r && other.g == g && other.b == b && other.a == a;
}

const Color Color::RED{ 0xFF, 0x00, 0x00, 0xFF };
const Color Color::GREEN{ 0x00, 0xFF, 0x00, 0xFF };
const Color Color::BLUE{ 0x00, 0x00, 0xFF, 0xFF };
const Color Color::BLACK{ 0x00, 0x00, 0x00, 0xFF };
const Color Color::WHITE{ 0xFF, 0xFF, 0xFF, 0xFF };
///
// Colorf
///
Colorf::Colorf ( float _r, float _g, float _b, float _a )
    : SDL_FColor{ std::fmod ( _r, 256.f ), std::fmod ( _g, 256.f ), std::fmod ( _b, 256.f ), std::fmod ( _a, 256.f ) } {
}
void Colorf::red ( float _r ) { r = std::fmod ( _r, 256.f ); }
void Colorf::green ( float _g ) { g = std::fmod ( _g, 256.f ); }
void Colorf::blue ( float _b ) { b = std::fmod ( _b, 256.f ); }
void Colorf::alpha ( float _a ) { a = std::fmod ( _a, 256.f ); }
void Colorf::rgba ( float _r, float _g, float _b, float _a ) {
  red ( _r );
  green ( _g );
  blue ( _b );
  alpha ( _a );
}
float Colorf::red ( ) const { return r; }
float Colorf::green ( ) const { return g; }
float Colorf::blue ( ) const { return b; }
float Colorf::alpha ( ) const { return a; }
Colorf::operator uint32_t ( ) const {
  return ( static_cast< uint32_t > ( red ( ) * 255 ) << 24 ) | ( static_cast< uint32_t > ( green ( ) * 255 ) << 16 ) |
         ( static_cast< uint32_t > ( blue ( ) * 255 ) << 8 ) | static_cast< uint32_t > ( alpha ( ) * 255 );
}
bool Colorf::operator== ( const Colorf &other ) const {
  return std::fabs ( other.r - r ) <= std::numeric_limits< float >::epsilon ( ) &&
         std::fabs ( other.g - g ) <= std::numeric_limits< float >::epsilon ( ) &&
         std::fabs ( other.b - b ) <= std::numeric_limits< float >::epsilon ( ) &&
         std::fabs ( other.a - a ) <= std::numeric_limits< float >::epsilon ( );
}
const Colorf Colorf::RED{ 1.f, 0.f, 0.f, 1.f };
const Colorf Colorf::GREEN{ 0.f, 1.f, 0.f, 1.f };
const Colorf Colorf::BLUE{ 0.f, 0.f, 1.f, 1.f };
const Colorf Colorf::BLACK{ 0.f, 0.f, 0.f, 1.f };
const Colorf Colorf::WHITE{ 1.f, 1.f, 1.f, 1.f };

///
// Keyboard
///
bool KB::keyboard ( const SDL_Scancode scancode ) noexcept {
  static const bool *state = ::SDL_GetKeyboardState ( nullptr );
  return state[ scancode ] != 0;
}
bool KB::up ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_UP ); }
bool KB::down ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_DOWN ); }
bool KB::left ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_LEFT ); }
bool KB::right ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_RIGHT ); }
bool KB::space ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_SPACE ); }
bool KB::w ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_W ); }
bool KB::a ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_A ); }
bool KB::d ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_D ); }
bool KB::s ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_S ); }
bool KB::one ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_1 ); }
bool KB::two ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_2 ); }
bool KB::three ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_3 ); }
bool KB::four ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_4 ); }
bool KB::five ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_5 ); }
bool KB::six ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_6 ); }
bool KB::seven ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_7 ); }
bool KB::eight ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_8 ); }
bool KB::nine ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_9 ); }
bool KB::zero ( ) noexcept { return KB::keyboard ( SDL_SCANCODE_0 ); }
///
// Mouse
///

int32_t MOUSE::mouse ( const MouseStateType type ) noexcept {
  switch ( type ) {
    case MouseStateType::None:
      return ::SDL_GetMouseState ( nullptr, nullptr );
      break;
    case MouseStateType::Relative:
      return ::SDL_GetRelativeMouseState ( nullptr, nullptr );
      break;
    case MouseStateType::Global:
      return ::SDL_GetGlobalMouseState ( nullptr, nullptr );
      break;
    default:
      break;
  }
  return 0;
}

bool MOUSE::left ( ) noexcept { return MOUSE::mouse ( ) & SDL_BUTTON_LEFT; }
bool MOUSE::right ( ) noexcept { return MOUSE::mouse ( ) & SDL_BUTTON_RIGHT; }
bool MOUSE::middle ( ) noexcept { return MOUSE::mouse ( ) & SDL_BUTTON_MIDDLE; }
auto MOUSE::position ( const MouseStateType type ) noexcept -> std::tuple< float, float > {
  float x{ 0 };
  float y{ 0 };

  switch ( type ) {
    case MouseStateType::None:
      ::SDL_GetMouseState ( &x, &y );
      break;
    case MouseStateType::Relative:
      ::SDL_GetRelativeMouseState ( &x, &y );
      break;
    case MouseStateType::Global:
      ::SDL_GetGlobalMouseState ( &x, &y );
      break;
    default:
      break;
  }
  return std::make_tuple ( x, y );
}
bool MOUSE::left_relative ( ) noexcept { return MOUSE::mouse ( MouseStateType::Relative ) & SDL_BUTTON_LEFT; }
bool MOUSE::right_relative ( ) noexcept { return MOUSE::mouse ( MouseStateType::Relative ) & SDL_BUTTON_RIGHT; }
bool MOUSE::middle_relative ( ) noexcept { return MOUSE::mouse ( MouseStateType::Relative ) & SDL_BUTTON_MIDDLE; }
auto MOUSE::position_relative ( ) noexcept -> std::tuple< float, float > {
  return MOUSE::position ( MouseStateType::Relative );
}

bool MOUSE::left_global ( ) noexcept { return MOUSE::mouse ( MouseStateType::Global ) & SDL_BUTTON_LEFT; }
bool MOUSE::right_global ( ) noexcept { return MOUSE::mouse ( MouseStateType::Global ) & SDL_BUTTON_RIGHT; }
bool MOUSE::middle_global ( ) noexcept { return MOUSE::mouse ( MouseStateType::Global ) & SDL_BUTTON_MIDDLE; }
auto MOUSE::position_global ( ) noexcept -> std::tuple< float, float > {
  return MOUSE::position ( MouseStateType::Global );
}

void MOUSE::enable_mouse_relative ( SDL_Window *window, const bool enable ) noexcept {
  // While in Relative Mode, the cursor is hidden.
  ::SDL_SetWindowRelativeMouseMode ( window, enable );
}
void MOUSE::enable_mouse_grab ( SDL_Window *window, const bool enable ) noexcept {
  ::SDL_SetWindowRelativeMouseMode ( window, enable );
  ::SDL_SetWindowMouseGrab ( window, enable );
}

void MOUSE::enable_mouse_cursor ( const bool enable ) noexcept { enable ? ::SDL_ShowCursor ( ) : ::SDL_HideCursor ( ); }

}  // namespace sdl
