# GD

This is a little helper lib to try to push out games... Also the last of any C++ I do these days.  There is ALOT of work that needsd to happen and I have zero time atm. Lets see how it goes!!!

## Dependenices

This following depends on the great SDL3 suite, physfs, and has support for the awesome ImGUI

To build the apps associated with this repo.

    cmake ../ -DGD_BUILD_APPS=1 

This will pull down all the dependencies and build / link it. 

### External Submodule ( Good for developing with your game )

You can also use this as an external dependency to your project via git submodule 

```
mkdir -p mynewproject/external

git submodule add https://gitlab.com/begui/gd.git mynewproject/external

```

#### Create our cmake file

    touch CMakeLists.txt

Add the following 

```
cmake_minimum_required(VERSION 3.22 )
project(mynewproject VERSION 0.0.0)
set(GD_USE_IMGUI  ON) # Enables ImGUI support

set(CMAKE_VERBOSE_MAKEFILE OFF PARENT)
# Enable export of compile commands (for tools like clangd)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
# Set C++ Stuff
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(GD_PROJECT_DIR ${CMAKE_SOURCE_DIR}/external/gd) #hack
add_subdirectory(${CMAKE_SOURCE_DIR}/external/gd EXCLUDE_FROM_ALL) 

set(${PROJECT_NAME}_SRC
  ${CMAKE_SOURCE_DIR}/src/main.cc
)

add_gd_library(${PROJECT_NAME} "${${PROJECT_NAME}_SRC}")

install (TARGETS ${PROJECT_NAME} DESTINATION ${CMAKE_BINARY_DIR})



```


